require('./bootstrap');
window.Vue = require('vue');
import router from './router.js'

const app = new Vue({
    el: '#app',
    router,
});