@extends('layouts.app')

@section('content')
<div class="col-12 col-md-12 col-lg-12">
    <div class="card">
        <form method="post" class="needs-validation" enctype= multipart/form-data novalidate="" action="{{ route('permission-add-post') }}">
          @csrf
          <div class="card-body">
          	<div class="row">                               
              <div class="form-group col-md-12 col-12">
                	<label>Name</label>
                	<input type="text" class="form-control" name="name" value="" required="required">
                	<div class="invalid-feedback">
                  	Please fill in the name
                	</div>
              </div>
            </div>
            <div class="row">                               
              <div class="form-group col-md-12 col-12">
                	<label>Group Name</label>
                	<input type="text" class="form-control" name="group_name" value="" required="required">
                	<div class="invalid-feedback">
                  	Please fill in the name
                	</div>
              </div>
            </div>
          </div>
          <div class="card-footer text-right">
            <button class="btn btn-primary">Submit</button>
          </div>
        </form>
    </div>
</div>
@endsection