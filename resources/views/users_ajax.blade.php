<table class="table table-striped table-md dataTable-{{$status2}}">
    <thead>
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Privileges</th>
            <th>Created Date</th>
            <th><div align="right">@if(auth()->user()->can('add users') && $status2=='active')<a href="{{ route('users-add') }}" class="btn btn-md btn-primary">Create New</a>@endif</div></th>
        </tr>
    </thead>
    @php
    $no=1;
    @endphp
    @if(count($data) > 0)
    <tbody>
    @foreach($data as $data_value)
    @php
        $role_loop = 1;
        $user_role = "";
        $user = App\User::where('id',$data_value->id)->first();
    @endphp
    <tr>
        <td>{{ $no }}</td>
        <td>{{ $data_value->Users_info->first_name }} {{ $data_value->Users_info->last_name }}</td>
        <td>{{ $data_value->email }}</td>
        <td>
            @foreach($role as $value)
                @if($user->hasRole($value->name))
                    @php
                        if($role_loop==1){
                            $separator = "";
                        }
                        else{
                            $separator = ", ";
                        }
                        $user_role = $user_role.$separator.$value->name;
                        $role_loop++;
                    @endphp
                @endif
            @endforeach
            {{$user_role}}
        </td>
        <td>{{ date_with_hours($data_value->created_at) }}</td>
        <td>
            <div align="right">
                @if(auth()->user()->can('edit users') && $status2=='active')
                    <a class="btn btn-icon btn-primary" href='{{ route("users-edit",["status" => $status2, "id" => $data_value->id]) }}'><i class="far fa-edit"></i></a>
                @endif
                
                @if(auth()->user()->can('delete users') && ($status2=='active' || $status2=='pending'))
                    <a href="#" class="btn btn-icon btn-danger" onclick='confirmDelete("{{ route("users-delete",["status" => $status2, "id" => $data_value->id]) }}")'><i class="fas fa-times"></i></a>
                @endif

                @if(auth()->user()->can('delete users') && $status2=='inactive')
                    <a href="#" class="btn btn-icon btn-danger" onclick='confirmDelete("{{ route("users-delete",["status" => "inactive", "id" => $data_value->id]) }}")'>Restore</a>
                @endif
            </div>
        </td>
    </tr>
    @php
    $no++;
    @endphp
    @endforeach
    </tbody>
    @else
        <tr>
            <td colspan="5"><div align="center">-No Data-</div></td>
        </tr>
    @endif
</table>
@include('dataTable')