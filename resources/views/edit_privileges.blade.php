@extends('layouts.app')

@section('content')
<div class="col-12 col-md-12 col-lg-12">
    <div class="card">
        <form method="post" class="needs-validation" enctype= multipart/form-data novalidate="" action="{{ route('privileges-edit-post',['status' => 'active', 'id' => $role->id]) }}">
          @csrf
          <div class="card-body">
          	<div class="row">                               
              <div class="form-group col-md-12 col-12">
                	<label>Name</label>
                	<input type="text" class="form-control" name="name" value="{{ $role->name }}" required="">
                	<div class="invalid-feedback">
                  	Please fill in the name
                	</div>
              </div>
            </div>
            <div class="row"> 
              @foreach($permission as $permission_key => $permission_value)
              <div class="form-group col-sm-12 col-md-2 col-lg-12" style="background-color:#f2eded;margin-right:10px;padding-top:10px;padding-bottom:10px;">
                <div align="center" style="background-color:#203e7b;color:white;">{{ strtoupper($permission_value->group_name) }}</div><br>
                  <div class="row"> 
                    @foreach($permission_value->Child as $child_key => $child_value)
                      @php
                        $permission_data = Spatie\Permission\Models\Permission::where('id',$child_value->id)->first();
                        $checked = '';
                        if($role->hasPermissionTo($permission_data->name)){
                          $checked = "checked='checked'";
                        }
                      @endphp
                      <div class="form-group col-sm-12 col-md-2 col-lg-2" style="color:white;background-color:#7094dd;margin-bottom:20px;margin-left:10px;margin-right:10px;padding-top:10px;padding-bottom:10px;">
                        <input type="checkbox" {{$checked}} name="privileges[]" style="margin-right:10px;transform: scale(1.5);" value="{{$child_value->name}}">
                        {{ucwords($child_value->name)}}
                      </div>
                    @endforeach
                  </div>
              </div>
              @endforeach
            </div>
          </div>
          <div class="card-footer text-right">
            <button class="btn btn-primary">Submit</button>
          </div>
        </form>
    </div>
</div>
@endsection