<!-- General CSS Files --> 
<link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/modules/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/modules/fontawesome/css/all.min.css">

<!-- Select2 -->
<link type="text/css" rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/css/select2.min.css" />

<!-- Date -->
<link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/modules/bootstrap-daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/modules/bootstrap-timepicker/css/bootstrap-timepicker.min.css">

<!-- Template CSS -->
<link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/css/style.css">
<link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/css/components.css">

<!--SWEET ALERT-->
<link href="{{ config('constant.ASSETS_URL') }}backend/plugin/sweetalert/sweetalert.css" rel="stylesheet" >

<!--FILE UPLOAD-->
<link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/css/bootstrap-fileupload.min.css"/>

<!--CUSTOM CSS-->
<link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/css/custom.css"/>

<!--FANCYBOX CSS-->
<link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/css/jquery.fancybox.min.css">

<!--SUMMERNOTE / TEXT EDITOR-->
<link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/modules/summernote/summernote-bs4.css">

@if(isset($page) && ($page=='index' || $page=='ConsumerLetter::detail_consumer_letter' ))
  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/modules/jqvmap/dist/jqvmap.min.css">
  <link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/modules/summernote/summernote-bs4.css">
  <link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/modules/owlcarousel2/dist/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="{{ config('constant.ASSETS_URL') }}backend/modules/owlcarousel2/dist/assets/owl.theme.default.min.css">
@endif

@if(isset($page) && ($page=='index' || $page=='Inbox::data' ))
	<link href="{{ config('constant.ASSETS_URL') }}backend/css/chat.css" rel="stylesheet"/> 
@endif
