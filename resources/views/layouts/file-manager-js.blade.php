<script>
var global_app_url = "{{config('constant.APP_URL')}}";

function file_manager_modal_add_show(config)
{
    $.ajax({
        url: global_app_url+"/file-manager/add",
        type: "get",
        data: {
            config: config
        },
        beforeSend: function () {
        
        },
        error: function() {
        
        },
        success: function (html) {
            $('.modal-add-image').modal('show');
            $(".modal-add-content").html("");
            $(".modal-add-content").html(html);
        }
    });
}

function file_manager_edit(id,type,config)
{
    $.ajax({
        url: global_app_url+"/file-manager/edit",
        type: "get",
        data: {
            id: id,
            type: type,
            config: config
        },
        beforeSend: function () {
        
        },
        error: function() {
        
        },
        success: function (html) {
            $('.modal-detail-data').modal('show');
            $(".modal-detail-content").html("");
            $(".modal-detail-content").html(html);
            
        }
    });
}

function file_manager_show(config)
{
    file_manager_data("{{ route('file-manager-get',['type' => 'image','page' => 1, 'order' => 'desc']) }}",'image',config);
    $('.bd-example-modal-lg').modal('show');
}

function file_manager_data(route,type,config)
{
    $.ajax({
        url: route,
        type: "get",
        data: {
            config : config
        },
        beforeSend: function () {
        $("."+type+"-box-modal").html("<div style='width:100%;' class='text-center'><br><div class='fa fa-spinner fa-spin fa-3x'></div></div>");
        },
        error: function() {
        
        },
        success: function (html) {
            $("."+type+"-box-modal").html("");
            $(html).hide().appendTo("."+type+"-box-modal").fadeIn(1000);
        }
    });
}

function file_manager_go(filename,caption,config,file_manager_id)
{
    $('.bd-example-modal-lg').modal('toggle');
    $.ajax({
        url: global_app_url+"/file-manager/go",
        type: "get",
        data: {
            filename : filename,
            caption : caption,
            config : config,
            file_manager_id : file_manager_id
        },
        beforeSend: function () {
        
        },
        error: function() {
        
        },
        success: function (response) {
            var json = JSON.parse(response);
            if(!json.editor){
                if(json.result_validation){
                    $("."+json.container).html("");
                    if($("."+json.container+"-default-image").length > 0){
                        $("."+json.container+"-default-image").hide();
                    }
                    $("."+json.container).html(json.html);
                }
                else{
                    $("."+json.container+"-default-image").show();
                    swal("Oppss...", json.message, "warning");
                }
            }
            else{
                $('.summernote').summernote('editor.pasteHTML', json.html);
            }
        }
    });
}

function file_manager_get_data(app_url,type,page,config)
{
    var order = document.getElementById('order-'+type).value;
    var key = document.getElementById('search-'+type).value;
    file_manager_data(app_url+"/file-manager/get/"+type+"/"+page+"/"+order+"/"+key,type,config);
}

/*FIRST LOAD MODAL*/
// $('.modal-add-image').on('show.bs.modal', function (event){
//     var config = false;
//     if($(".file-manager-data").length > 0){
//         var config = true;
//     }
//     file_manager_data("{{ route('file-manager-get',['type' => 'image','page' => 1, 'order' => 'desc']) }}",'image',config);
// })

// $('body').on('click', 'a.filemanager-image', function(e) {
//     file_manager_data("{{ route('file-manager-get',['type' => 'image','page' => 1, 'order' => 'desc']) }}",'image');
// });

// $('body').on('click', 'a.filemanager-video', function(e) {
//     file_manager_data("{{ route('file-manager-get',['type' => 'video','page' => 1, 'order' => 'desc']) }}",'video');
// });

// $('body').on('click', 'a.filemanager-others', function(e) {
//     file_manager_data("{{ route('file-manager-get',['type' => 'others','page' => 1, 'order' => 'desc']) }}",'others');
// });

$('body').on('click', '.container-image .pagination a', function(e) {
    e.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    var config =  document.getElementById('config-data').value;
    file_manager_get_data(global_app_url,'image',page,config);
});

// $('body').on('click', '.container-video .pagination a', function(e) {
//     e.preventDefault();
//     var page = $(this).attr('href').split('page=')[1];
//     file_manager_get_data(global_app_url,'video',page);
// });

// $('body').on('click', '.container-others .pagination a', function(e) {
//     e.preventDefault();
//     var page = $(this).attr('href').split('page=')[1];
//     file_manager_get_data(global_app_url,'others',page);
// });

// $('body').on('click', '.container-others .pagination a', function(e) {

// });

// function file_manager_store(app_url)
// {
//     let formData = new FormData($('#file-manager-form-add')[0]);
//     let file = $('input[type=file]')[0].files[0];
//     let size = file.size;
//     formData.append('upfile', file, file.name);
//     var config = document.getElementById('config-data').value;
//     formData.append('config', config);

//     var limit_size = "{{config('constant.MAX_SIZE_IMAGE_UPLOAD')*1000000}}";
//     if(parseInt(size) > parseInt(limit_size)){
//         swal("Oppss...", "Ukuran maksimal adalah : "+parseInt(limit_size)/1000000+" MB", "warning");
//     }
//     else{
//         $.ajax({
//         type        : 'POST',
//         url         : "{{route('file-manager-store')}}",
//         contentType : false,
//         processData : false,   
//         cache       : false,      
//         data        : formData,
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         },
//         error: function (request, error) {
//         },
//         success: function (response) {
//             var json = JSON.parse(response);
//             if(json.status=="ok"){
//                 $('.modal-add-image').modal('toggle');
//                 file_manager_data(app_url+"/file-manager/get/image/1/desc/",'image',json.config);
//             }
//             else{
//                 swal("Oppss...", response, "warning");
//             }
//         }
//         });
//     }
// }

// $(document).ready(function() {
//     $('.file-manager-form-add').bind('submit',function(){
//         file_manager_store(global_app_url);
//     });
// });
</script>