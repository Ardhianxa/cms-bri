@include('segment.file-manager-edit-form')
@include('segment.file-manager-add-form')
<div class="modal fade bd-example-modal-lg" id="bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-file-manager-div-active"></div>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#203e7b;">
                <h4 class="modal-title" id="myLargeModalLabel" style="padding-bottom:20px; color:#ffffff;">FILE MANAGER</h4>
                <button type="button" class="close close2" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- <ul class="nav nav-pills" id="myTab3" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link filemanager-image active" id="image-tab3" data-toggle="tab" href="#image" role="tab" aria-controls="image" aria-selected="true">Image</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link filemanager-video" id="video-tab3" data-toggle="tab" href="#video" role="tab" aria-controls="video" aria-selected="false">Video</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link filemanager-others" id="others-tab3" data-toggle="tab" href="#others" role="tab" aria-controls="others" aria-selected="false">Others</a>
                    </li>
                </ul> -->
                <div class="tab-content" id="myTabContent2">
                    <div class="tab-pane fade show active" id="image" role="tabpanel" aria-labelledby="image-tab3">
                        <div class="row image-box-modal">
                        
                        </div>
                    </div>
                    <!-- <div class="tab-pane fade show" id="video" role="tabpanel" aria-labelledby="video-tab3">
                        <div class="row video-box-modal">
                        
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="others" role="tabpanel" aria-labelledby="others-tab3">
                        <div class="row others-box-modal">
                        
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>