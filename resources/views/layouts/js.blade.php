<!-- General JS Scripts --> 
@if(isset($page) and $page=='Inbox::data') 
  <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
@else
  <script src="{{ config('constant.ASSETS_URL') }}backend/modules/jquery.min.js"></script> 
@endif

<script src="{{ config('constant.ASSETS_URL') }}backend/modules/popper.js"></script>
<script src="{{ config('constant.ASSETS_URL') }}backend/modules/tooltip.js"></script>

<script src="{{ config('constant.ASSETS_URL') }}backend/modules/bootstrap/js/bootstrap.min.js"></script>
@yield('extend_code_2')
<script src="{{ config('constant.ASSETS_URL') }}backend/modules/nicescroll/jquery.nicescroll.min.js"></script>
<script src="{{ config('constant.ASSETS_URL') }}backend/modules/moment.min.js"></script>
<script src="{{ config('constant.ASSETS_URL') }}backend/js/stisla.js"></script>

<!-- Date -->
<script src="{{ config('constant.ASSETS_URL') }}backend/modules/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- FILE UPLOAD -->
<script src="{{ config('constant.ASSETS_URL') }}backend/js/bootstrap-fileupload.js"></script>

<!-- Template JS File -->
<script src="{{ config('constant.ASSETS_URL') }}backend/js/scripts.js"></script>
<script src="{{ config('constant.ASSETS_URL') }}backend/js/custom.js"></script>

<!-- Bootstrap Modal -->
<script src="{{ config('constant.ASSETS_URL') }}backend/js/page/bootstrap-modal.js"></script>
@include('layouts.file-manager-js')
@yield('extend_code')
<!--SWEET ALERT-->
<script src="{{ config('constant.ASSETS_URL') }}backend/plugin/sweetalert/sweetalert-dev.js"></script>

<!--HELPERS JS-->
<script type="text/javascript" src="{{ config('constant.ASSETS_URL') }}backend/js/helpers.js"></script> 
<script type="text/javascript" src="{{ config('constant.ASSETS_URL') }}backend/js/helpers-custom.js"></script> 

<!--FANCY BOX JS-->
<script src="{{ config('constant.ASSETS_URL') }}backend/js/jquery.fancybox.min.js"></script>

<!--SUMMERNOT / TEXTEDITOR-->
<script src="{{ config('constant.ASSETS_URL') }}backend/modules/summernote/summernote-bs4.js"></script>

@yield('before_sumit')
<script>
    $(document).ready(function() {
      $('[class^=date]').keydown(validateDateField);

      $('.date').daterangepicker({
        locale: {format: 'DD-MM-YYYY'},
        singleDatePicker: true
      });

      var readURL = function(input) {
        if (input.files && input.files[0]) {
          var reader = new FileReader();
    
          reader.onload = function (e) {
            $('.profile-pic').attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[0]);
        }
      }
      
      $(".file-upload").on('change', function(){
        readURL(this);
      });
      
      $(".upload-button").on('click', function() {
         $(".file-upload").click();
      });

      // $body = $("body");
      // $(document).on({
      //     ajaxStart: function() { $body.addClass("loading");},
      //     ajaxStop: function() { $body.removeClass("loading");}    
      // });
    });
</script>

<div class="showcase sweet" style="display:none;"><a id="failed">.</a></div>
@if(Session::has('flash_success'))
  <script>
  $(document).ready(function(e){
      $('#failed').click();
  });

  document.querySelector('.showcase.sweet a').onclick = function(){
  swal("Congratulations...", "{{ Session::get('flash_success') }}", "success");
  };
  </script>
@endif

@if(Session::has('flash_failed'))
  <script>
  $(document).ready(function(e){
      $('#failed').click();
  });

  document.querySelector('.showcase.sweet a').onclick = function(){
  swal("Oops...", "{{ Session::get('flash_failed') }}", "error");
  };
  </script>
@endif

@if(isset($page) and $page=='index')
    <!-- JS Libraies -->
    <script src="{{ config('constant.ASSETS_URL') }}backend/modules/jquery.sparkline.min.js"></script>
    <script src="{{ config('constant.ASSETS_URL') }}backend/modules/chart.min.js"></script>
    <script src="{{ config('constant.ASSETS_URL') }}backend/modules/owlcarousel2/dist/owl.carousel.min.js"></script>
    <script src="{{ config('constant.ASSETS_URL') }}backend/modules/summernote/summernote-bs4.js"></script>
    <script src="{{ config('constant.ASSETS_URL') }}backend/modules/chocolat/dist/js/jquery.chocolat.min.js"></script>

    <!-- Page Specific JS File -->
    <script src="{{ config('constant.ASSETS_URL') }}backend/js/page/index.js"></script>
@endif

@if(isset($page) && $page=='ConsumerLetter::detail_consumer_letter')
  <!-- JS Libraies -->
  <script src="{{ config('constant.ASSETS_URL') }}backend/modules/summernote/summernote-bs4.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDo1P6Gp5Z7jl_ICiC1klaNj_jGLfHKn_Q&callback=initialize"></script>

  <script type="text/javascript"> 
  var map = null;
  var marker = null;
  var assets_url = "{{ config('constant.ASSETS_URL') }}";

  var infowindow = new google.maps.InfoWindow(
  { 
      size: new google.maps.Size(200,100)
  });

  function initialize() {
      if(navigator.geolocation) 
      {
          var lon = "{{$data->lon}}";
          var lat = "{{$data->lat}}";
          var currenct_LatLng = { lat: parseFloat(lat), lng: parseFloat(lon) };
          var myOptions = {
              zoom: 15,
              center: currenct_LatLng,
              mapTypeControl: true,
              mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
              navigationControl: true,
              mapTypeId: google.maps.MapTypeId.ROADMAP
          }
          map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
          
          google.maps.event.addListener(map, 'click', function() {
              infowindow.close();
          });

          
          var marker_latlong = new google.maps.LatLng('{{$data->lat}}', '{{$data->lon}}');
          var marker_pembelian = createMarker(map,marker_latlong, "<font color='#000000'><b>Location</b></font><br><font color='#000000'>"+marker_latlong+"</font>", "","detail");
      }
      else
      {
          alert('Geo Location feature is not supported in this browser.');
      }
  }
  </script>
@endif

<script>
$(function() {
  if($(".datetimepicker").length) {
    $('.datetimepicker').daterangepicker({
      singleDatePicker: true,
      locale: {format: 'DD-MM-YYYY hh:mm'},
      timePicker: true,
      timePicker24Hour: true,
    });
  }
});
</script>

<script type="text/javascript" src="{{ config('constant.ASSETS_URL') }}backend/js/select2.js"></script>
<script type="text/javascript">
$.fn.select2.amd.require([
  "select2/core",
  "select2/utils",
  "select2/compat/matcher"
], function (Select2, Utils, oldMatcher) {
    var $tags = $(".js-example-tags");
    $tags.select2({tags:['']});

});
</script>