<!DOCTYPE html>  
<html lang="en"> 
<head> 
  <title>{{ isset($title) ? $title : "CMS" }}</title> 
  @include('layouts.metadata')
  @include('layouts.css')
</head>
<body
@if(isset($page) && $page=='ConsumerLetter::detail_consumer_letter')
  onload="initialize()"
@endif
>
  <div id="app">
    @if((!isset($page) || $page=='login' || $page=='forgot_password') || $page == '404')
      @yield('content')
    @else
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
          </ul>
        </form>
        @include('layouts.profile')
      </nav>
      @include('layouts.menu')
      @include('layouts.file-manager-modal')
      <div class="main-content">
        <section class="section">
            @include('layouts.section_header')
            @yield('content')
        </section>
      </div>
      @include('layouts.footer')
    </div>
    @endif
  </div>
  @include('layouts.js')
  @yield('modal-data')
</body>
</html>