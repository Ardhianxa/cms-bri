<div class="main-sidebar sidebar-style-2"> 
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="{{ route('home-admin') }}">BRI-NEWS</a>  
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{ route('home-admin') }}">BRI</a>
    </div>
    <ul class="sidebar-menu">

      
      <li class="menu-header">Dashboard</li>

      <li class="@if(strtolower(Request::segment(1))=='home') active @endif"><a class="nav-link" href="{{ route('home-admin') }}"><i class="far fa-user"></i> <span>Dashboard</span></a></li>
      
      <li class="menu-header">Users</li>

      @if(auth()->user()->can('view users'))
        <li class="@if(strtolower(Request::segment(1))=='users') active @endif"><a class="nav-link" href="{{ route('users',['status' => 'active']) }}"><i class="far fa-user"></i> <span>Users</span></a></li>
      @endif

      @if(auth()->user()->can('view privileges'))
        <li class="@if(strtolower(Request::segment(1))=='privileges') active @endif"><a class="nav-link" href="{{ route('privileges',['status' => 'active']) }}"><i class="fas fa-universal-access"></i> <span>Privileges</span></a></li>
      @endif

      @if(request()->getHttpHost()=="local.bri.id")
        @if(auth()->user()->can('view permission'))
          <li class="@if(strtolower(Request::segment(1))=='permission') active @endif"><a class="nav-link" href="{{ route('permission') }}"><i class="fas fa-universal-access"></i> <span>Permission</span></a></li>
        @endif
      @endif

      <li class="menu-header">Content</li>
      @if(auth()->user()->can('view article'))
        <li class="@if(strtolower(Request::segment(1))=='article') active @endif"><a class="nav-link" href="{{ route('article',['status' => 'active']) }}"><i class="fas fa-newspaper"></i> <span>Article</span></a></li>
      @endif

      <li class="menu-header">Distribution</li>
      @if(auth()->user()->can('view channel'))
        <li class="@if(strtolower(Request::segment(1))=='channel') active @endif"><a class="nav-link" href="{{ route('channel') }}"><i class="fas fa-tags"></i> <span>Channel</span></a></li>
      @endif
    </ul>        
  </aside>
</div>