@extends('layouts.app') 

@section('content')
<div class="section-body">
<div class="row">
	<div class="col-12 col-md-12 col-lg-12">
		<div class="card">
            <div class="card-body">
                <ul class="nav nav-pills" id="myTab3" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link @if($status=='active') active @endif" id="tab-active" data-toggle="tab" href="#active" role="tab" aria-controls="active" aria-selected="true">Active</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link @if($status=='pending') active @endif" id="tab-pending" data-toggle="tab" href="#pending" role="tab" aria-controls="contact" aria-selected="false">Pending</a>
                  </li> 
                  <li class="nav-item">
                    <a class="nav-link @if($status=='inactive') active @endif" id="tab-inactive" data-toggle="tab" href="#inactive" role="tab" aria-controls="contact" aria-selected="false">Inactive</a>
                  </li> 
                </ul>
                <div class="tab-content" id="myTabContent2">
                  	<div class="tab-pane fade show @if($status=='active') active @endif" id="active" role="tabpanel" aria-labelledby="tab-active"><br>
                        <div class="table-responsive data-active"></div>
                  	</div>
                  	<div class="tab-pane fade show @if($status=='inactive') active @endif" id="inactive" role="tabpanel" aria-labelledby="tab-inactive"><br>
                        <div class="table-responsive data-inactive"></div>
                  	</div>
                  	<div class="tab-pane fade show @if($status=='pending') active @endif" id="pending" role="tabpanel" aria-labelledby="tab-pending"><br>
                        <div class="table-responsive data-pending"></div>
                  	</div>
                </div>
             </div>
        </div>
	</div>
</div>
</div>
@endsection

@section('extend_code_2')
<script type="text/javascript">
	$(document).ready(function() {
		ajax_get_data("{{$page}}","{{$key}}","{{$status}}","data-active","active","{{ route('ajax-get-data') }}");
		ajax_get_data("{{$page}}","{{$key}}","{{$status}}","data-inactive","inactive","{{ route('ajax-get-data') }}");
		ajax_get_data("{{$page}}","{{$key}}","{{$status}}","data-pending","pending","{{ route('ajax-get-data') }}");
	});
</script>
@endsection

