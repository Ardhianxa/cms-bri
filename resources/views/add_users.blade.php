@extends('layouts.app')

@section('content')
<div class="col-12 col-md-12 col-lg-12">
    <div class="card">
        <form method="post" id="myform" class="needs-validation" enctype= multipart/form-data novalidate="" action="{{ route('users-add-post') }}">
          @csrf
          <div class="card-body">
              	<div class="row">                               
                  <div class="form-group col-md-12 col-12"> 
                    	<label>Email</label>
                    	<input type="text" class="form-control" name="email" value="" required="">
                    	<div class="invalid-feedback">
                      	Please fill in the email
                    	</div>
                  </div>
              	</div>
              	<div class="row">
              		@foreach($role as $value)
                  <div align="center" class="form-group col-md-2 col-12 col-lg-2" style="margin-right:3px;">
                    	<label style="font-size:15px;">{{ ucwords(strtolower($value->name)) }}</label>
                    	<div style="margin-top:2px;">
                      	<input type="checkbox" name="privileges[]" class="form-control check" id="customCheck{{ $value->id }}" value="{{ $value->name }}">
                    </div>
                  </div>
                  @endforeach
              	</div>
          </div>
          <div class="card-footer text-right">
            <input type="submit" class="btn btn-primary" value="Submit">
          </div>
        </form>
    </div>
</div>
@endsection

@section('before_sumit')
<script>
$('#myform').submit(function() {
    var $checkboxes = $('#myform input[type="checkbox"]');
    var countCheckedCheckboxes = $checkboxes.filter(':checked').length;

    if(countCheckedCheckboxes==0){
      notif("Tentukan Hak Akses User Terlebih Dahulu","info");
      return false;
    }

    return true;
});
</script>
@endsection