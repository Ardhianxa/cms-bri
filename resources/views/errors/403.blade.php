@extends('layouts.app')

@section('content')
<section class="section">
	<div class="section-header">
	    <h1>PAGE NOT FOUND</h1>
	    <div class="section-header-breadcrumb">
	      <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
	      <div class="breadcrumb-item">PAGE NOT FOUND</div>
	    </div>
	</div>
    <div class="container mt-5">
        <div class="page-error">
          <div class="page-inner">
            <h1>403</h1>
            <div class="page-description">
              User does not have the right permissions.
            </div>
            <div class="page-search">
              <form>
                <div class="form-group floating-addon floating-addon-not-append">
                  <div class="input-group">
                    <div class="input-group-prepend">
                    </div>
                  </div>
                </div>
              </form>
              <div class="mt-3">
                <a href="{{ route('home') }}" class="btn btn-primary btn-lg btn-block" tabindex="4">
                  BACK TO HOME
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="simple-footer mt-5">
          Copyright &copy; Stisla 2018
        </div>
      </div>
</section>
@endsection