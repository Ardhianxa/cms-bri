@include('segment.file-manager-filter')
@if(count($data) > 0)
    @foreach($data as $key => $value)
        @if(Helpers::does_url_exists(config('constant.ASSETS_URL').'uploads/img/original/'.$value->filename))
            @php
                list($width, $height, $type, $attr) = getimagesize(config('constant.ACTUAL_PATH').'uploads/img/original/'.$value->filename);
            @endphp
            <div class="col-lg-3 col-md-3 col-sm-12">
                <div class="box-image" align="center">
                    @if(isset($config->multiple))
                        @if($config->multiple)
                            
                        @else
                            <a onclick="file_manager_go('{{$value->filename}}','{{$value->caption}}','{{json_encode($config)}}','{{$value->id}}')">
                                <img width="140px" src="{{ config('constant.ASSETS_URL') }}uploads/img/original/{{$value->filename}}">
                            </a>
                        @endif
                    @else
                        <a data-fancybox="gallery" href="{{ config('constant.ASSETS_URL') }}uploads/img/original/{{$value->filename}}">
                            <img width="140px" src="{{ config('constant.ASSETS_URL') }}uploads/img/original/{{$value->filename}}">
                        </a>
                    @endif
                    <div style="font-size:10px;margin-top:5px;margin-bottom:-30px;font-weight: 800;line-height: 1.35em;">
                        Width : {{$width}}px <br> Height : {{$height}}px
                    </div>
                    @if($value->created_id==Auth::id())
                    <br>
                    <a onclick="file_manager_edit('{{$value->id}}','image','{{json_encode($config)}}')" style="width:100%;margin-top:5px;" href="#" class="btn btn-sm btn-primary">Detail</a>
                    @endif
                </div>
            </div>
        @endif
    @endforeach
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="container-image">
            <input type="hidden" id="config-data" value="{{json_encode($config)}}">
            {{ $data->links('pagination.file-manager') }}
        </div>
    </div>
@endif
@include('segment.file-manager-add',['type' => 'image', 'config' => $config])