<li class="dropdown dropdown-list-toggle">
  <a href="#" data-toggle="dropdown" class="nav-link nav-link-lg message-toggle 
    @if(isset(request()->beep))
      beep
    @endif
    ">
    <i class="far fa-envelope"></i>
  </a>
  <div class="dropdown-menu dropdown-list dropdown-menu-right"> 
    <div class="dropdown-header" align="center">Messages</div>
    <div class="dropdown-list-content dropdown-list-message">
      @if(isset(request()->message))
        @foreach(request()->message as $message)
          <a href="{{ route('inbox',['id' => $message->sender_id]) }}" class="dropdown-item dropdown-item-unread">
            <div class="dropdown-item-avatar">
              <img alt="image" src="{{ Helpers::Image(config('constant.ASSETS_URL').'backend/img/users/'.$message->image) }}" class="rounded-circle">
            </div>
            <div class="dropdown-item-desc">
              <b>{{ $message->first_name.' '.$message->last_name }}</b>
              <p>New Message!</p>
              <div class="time">{{ comment_time(strtotime($message->updated_at)) }}</div>
            </div>
          </a>
        @endforeach
      @endif
    </div>
    <div class="dropdown-footer text-center">
      <a href="{{ route('inbox') }}">View All <i class="fas fa-chevron-right"></i></a>
    </div>
  </div>
</li>