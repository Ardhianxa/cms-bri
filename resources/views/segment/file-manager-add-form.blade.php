<div style="z-index:10000" class="modal fade modal-add-image" id="" tabindex="-1" role="dialog" aria-labelledby="modal-detail" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title" id="myLargeModalLabel" style="padding-bottom:20px;">ADD IMAGE FILE</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
      </div>
      <div class="modal-body">
        <div class="col-12 col-md-12 col-lg-12">
          <div class="card modal-add-content">
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>