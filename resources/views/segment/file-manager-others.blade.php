@include('segment.file-manager-filter',['type' => 'others'])
@if(count($data) > 0)
    @foreach($data as $key => $value)
        <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="box-image" align="center">
                <a target="_blank" href="{{ config('constant.ASSETS_URL') }}backend/uploads/original/{{$value->filename}}">
                    <img width="140px" src="{{ config('constant.ASSETS_URL') }}backend/img/file.png">
                </a>
                <br>
                <div class="box-image-title" style="padding-top:10px;">{{$value->title}}</div>
                <a data-toggle="modal" data-target=".modal-detail" style="width:100%;margin-top:5px;" href="#" class="btn btn-sm btn-primary">Detail</a>
            </div>
        </div>
    @endforeach
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="container-others">
            {{ $data->links('pagination.file-manager') }}
        </div>
    </div>
@endif
@include('segment.file-manager-add',['type' => 'others'])