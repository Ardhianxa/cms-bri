<li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
  <div class="d-sm-none d-lg-inline-block">Hi, {{ Session::get('first_name').' '.Session::get('last_name') }}</div></a>
  <div class="dropdown-menu dropdown-menu-right">
    <a href="{{ route('profile') }}" class="dropdown-item has-icon">
      <i class="far fa-user"></i> Profile
    </a>
    <!-- <a href="{{ route('activities', ['start-date' => date('d-m-Y'), 'end-date' => date('d-m-Y')]) }}" class="dropdown-item has-icon">
      <i class="fas fa-bolt"></i> Activities
    </a> -->
    <div class="dropdown-divider"></div>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="dropdown-item has-icon text-danger">
      <i class="fas fa-sign-out-alt"></i> Logout
    </a> 
  </div>
</li>