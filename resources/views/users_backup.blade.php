@extends('layouts.app') 

@section('content')
<div class="section-body">
<div class="row">
	<div class="col-12 col-md-12 col-lg-12">
		<div class="card">
            <div class="card-body">
                <ul class="nav nav-pills" id="myTab3" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link @if($status=='active') active @endif" id="tab-active" data-toggle="tab" href="#active" role="tab" aria-controls="active" aria-selected="true">Active</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link @if($status=='pending') active @endif" id="tab-pending" data-toggle="tab" href="#pending" role="tab" aria-controls="contact" aria-selected="false">Pending</a>
                  </li> 
                  <li class="nav-item">
                    <a class="nav-link @if($status=='inactive') active @endif" id="tab-inactive" data-toggle="tab" href="#inactive" role="tab" aria-controls="contact" aria-selected="false">Inactive</a>
                  </li> 
                </ul>
                <div class="tab-content" id="myTabContent2">
                  	<div class="tab-pane fade show @if($status=='active') active @endif" id="active" role="tabpanel" aria-labelledby="tab-active">
                      	<div class="card-body p-0 box-table-search">
			            	<div class="col-12 col-md-12 col-lg-3">
			                	<div class="input-group mb-3">
			                        <input type="text" class="form-control" id="search-active" placeholder="Search" aria-label="" value="@if($status=='active'){{ $key }}@endif">
			                        <div class="input-group-append">
			                          <a onclick="redirect('{{ config('constant.APP_URL') }}users/active/'+document.getElementById('search-active').value)" class="btn btn-lg btn-primary"><i class="fas fa-search"></i></a>
			                        </div>
		                      	</div>
			            	</div>
			            </div>
                        <div class="table-responsive">
		                  <table class="table table-striped table-md">
		                    <tr>
		                      <th width="5%">No</th>
		                      <th>Name</th>
		                      <th>Email</th>
		                      <th>Created Date</th>
		                      <th width="5%">@if(Helpers::privileges($menu_id,'add'))<a href="{{ route('users-add') }}" class="btn btn-md btn-primary">Create New</a>@endif</th>
		                    </tr>
		                    @php
		                    $no=1;
		                    @endphp
		                    @if(count($active) > 0)
		                    @foreach($active as $active_value)
		                    <tr>
		                      <td>{{ ($no+($active->perPage()*($active->currentPage()-1))) }}</td>
		                      <td>{{ $active_value->first_name }} {{ $active_value->last_name }}</td>
		                      <td>{{ $active_value->email }}</td>
		                      <td>{{ date_with_hours($active_value->created_at) }}</td>
		                      <td>
		                      	@if(Helpers::privileges($menu_id,'edit'))
		                      		<a class="btn btn-icon btn-primary" href='{{ route("users-edit",["status" => "active", "id" => $active_value->id]) }}'><i class="far fa-edit"></i></a>
		                      	@endif
		                      	@if(Helpers::privileges($menu_id,'delete'))
		                      		<a href="#" class="btn btn-icon btn-danger" onclick='confirmDelete("{{ route("users-delete",["status" => "active", "id" => $active_value->id]) }}")'><i class="fas fa-times"></i></a>
		                      	@endif
		                      </td>
		                    </tr>
		                    @php
		                    $no++;
		                    @endphp
		                    @endforeach
		                    @else
		                    	<tr>
		                    		<td colspan="5"><div align="center">-No Data-</div></td>
		                    	</tr>
		                    @endif
		                  </table>
		                </div>
                        <div class="card-footer text-right">
			                <nav class="d-inline-block">
			                  {{ $active->links() }}
			                </nav>
		              	</div>
                  	</div>
                  	<div class="tab-pane fade show @if($status=='inactive') active @endif" id="inactive" role="tabpanel" aria-labelledby="tab-inactive">
                    <div class="card-body p-0 box-table-search">
			            <div class="col-12 col-md-12 col-lg-3">
			                	<div class="input-group mb-3">
			                        <input type="text" class="form-control" id="search-inactive" placeholder="Search" aria-label="" value="@if($status=='inactive'){{ $key }}@endif">
			                        <div class="input-group-append">
			                          <a onclick="redirect('{{ config('constant.APP_URL') }}users/inactive/'+document.getElementById('search-inactive').value)" class="btn btn-lg btn-primary"><i class="fas fa-search"></i></a>
			                        </div>
		                      	</div>
			            	</div>
			            </div>
                        <div class="table-responsive">
		                  <table class="table table-striped table-md">
		                    <tr>
		                      <th width="5%">No</th>
		                      <th>Name</th>
		                      <th>Email</th>
		                      <th>Created Date</th>
		                      <th width="5%">Action</th>
		                    </tr>
		                    @php
		                    $no=1;
		                    @endphp
		                    @if(count($inactive) > 0)
		                    @foreach($inactive as $inactive_value)
		                    <tr>
		                      <td>{{ ($no+($inactive->perPage()*($inactive->currentPage()-1))) }}</td>
		                      <td>{{ $inactive_value->first_name }} {{ $inactive_value->last_name }}</td>
		                      <td>{{ $inactive_value->email }}</td>
		                      <td>{{ date_with_hours($inactive_value->created_at) }}</td>
		                      <td>
		                      	@if(Helpers::privileges($menu_id,'delete'))
		                      		<a href="#" class="btn btn-icon btn-danger" onclick='confirmDelete("{{ route("users-delete",["status" => "inactive", "id" => $inactive_value->id]) }}")'>Restore</a>
		                      	@endif
		                      </td>
		                    </tr>
		                    @php
		                    $no++;
		                    @endphp
		                    @endforeach
		                    @else
		                    	<tr>
		                    		<td colspan="5"><div align="center">-No Data-</div></td>
		                    	</tr>
		                    @endif
		                  </table>
		                </div>
                        <div class="card-footer text-right">
			                <nav class="d-inline-block">
			                  {{ $inactive->links() }}
			                </nav>
		              	</div>
                  	</div>
                  	<div class="tab-pane fade show @if($status=='pending') active @endif" id="pending" role="tabpanel" aria-labelledby="tab-pending">
                    <div class="card-body p-0 box-table-search">
			            <div class="col-12 col-md-12 col-lg-3">
			                	<div class="input-group mb-3">
			                        <input type="text" class="form-control" id="search-pending" placeholder="Search" aria-label="" value="@if($status=='pending'){{ $key }}@endif">
			                        <div class="input-group-append">
			                          <a onclick="redirect('{{ config('constant.APP_URL') }}users/pending/'+document.getElementById('search-pending').value)" class="btn btn-lg btn-primary"><i class="fas fa-search"></i></a>
			                        </div>
		                      	</div>
			            	</div>
			            </div>
                        <div class="table-responsive">
		                  <table class="table table-striped table-md">
		                    <tr>
		                      <th width="5%">No</th>
		                      <th>Name</th>
		                      <th>Email</th>
		                      <th>Created Date</th>
		                      <th width="10%">Action</th>
		                    </tr>
		                    @php
		                    $no=1;
		                    @endphp
		                    @if(count($pending) > 0)
		                    @foreach($pending as $pending_value)
		                    <tr>
		                      <td>{{ ($no+($pending->perPage()*($pending->currentPage()-1))) }}</td>
		                      <td>{{ $pending_value->first_name }} {{ $pending_value->last_name }}</td>
		                      <td>{{ $pending_value->email }}</td>
		                      <td>{{ date_with_hours($pending_value->created_at) }}</td>
		                      <td>
		                      	@if(Helpers::privileges($menu_id,'edit'))
		                      		<a class="btn btn-icon btn-primary" href='{{ route("users-edit",["status" => "pending", "id" => $pending_value->id]) }}'><i class="far fa-edit"></i></a>
		                      	@endif
		                      	@if(Helpers::privileges($menu_id,'delete'))
		                      		<a href="#" class="btn btn-icon btn-danger" onclick='confirmDelete("{{ route("users-delete",["status" => "pending", "id" => $pending_value->id]) }}")'><i class="fas fa-times"></i></a>
		                      	@endif
		                      </td>
		                    </tr>
		                    @php
		                    $no++;
		                    @endphp
		                    @endforeach
		                    @else
		                    	<tr>
		                    		<td colspan="5"><div align="center">-No Data-</div></td>
		                    	</tr>
		                    @endif
		                  </table>
		                </div>
                        <div class="card-footer text-right">
			                <nav class="d-inline-block">
			                  {{ $pending->links() }}
			                </nav>
		              	</div>
                  	</div>
                </div>
             </div>
        </div>
	</div>
</div>
</div>
@endsection