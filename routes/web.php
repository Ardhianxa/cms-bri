<?php   

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Illuminate\Support\Facades\Redis;
// Route::get('/', function () { 
//     echo Hash::make('masuk'); 
// });

Route::get('/', [
	'as' => 'home', 'uses' => 'Auth\LoginController@showLoginForm'
]);

/*ACTIVATION ACCOUNT*/
Route::get("users/activation/{token}" , [
	'as' => 'users-activation', 'uses' => 'UsersController@activation', 'middleware' => ['web','before_global','after_global']
]);
Route::post("users/activation/{token}" , [
	'as' => 'users-activation', 'uses' => 'UsersController@activationPost', 'middleware' => ['csrf','before_global','after_global']
]);

//test

//AFTER LOGIN
Route::group(['prefix' => '','middleware' => ['auth','session','before_global','after_global']], function ()
{
	/*HOME*/
	Route::get('dashboard', [
		'as' => 'home-admin', 'uses' => 'HomeController@index'
	]);

	/*PROFILE*/
	Route::get('profile', [
		'as' => 'profile', 'uses' => 'ProfileController@profile'
	]);
	Route::post('profile', [
		'as' => 'post.profile', 'uses' => 'ProfileController@profileUpdate'
	]);
	Route::get("activities/{start}/{end}" , [
		'as' => 'activities', 'uses' => 'ProfileController@activities'
	]);

	/*USERS*/
	Route::get("users/add" , [
		'as' => 'users-add', 'uses' => 'UsersController@add', 'middleware' => ['permission:add users']
	]);
	Route::post("users/add" , [
		'as' => 'users-add-post', 'uses' => 'UsersController@addPost', 'middleware' => ['permission:add users','csrf']
	]);
	Route::get("users/edit/{status}/{id}" , [
		'as' => 'users-edit', 'uses' => 'UsersController@edit', 'middleware' => ['permission:edit users']
	]);
	Route::post("users/edit/{status}/{id}" , [
		'as' => 'users-edit-post', 'uses' => 'UsersController@editPost', 'middleware' => ['permission:edit users']
	]);
	Route::post("users/delete/{status}/{key}" , [
		'as' => 'users-delete', 'uses' => 'UsersController@delete', 'middleware' => ['permission:delete users']
	]);
	Route::get("users/{status}/{key?}" , [
		'as' => 'users', 'uses' => 'UsersController@index', 'middleware' => ['permission:view users']
	]);
	Route::get('ajax', [
		'as' => 'ajax-get-data', 'uses' => 'UsersController@ajax', 'middleware' => ['permission:view users']
	]);

	/*PRIVILEGES*/
	Route::get("privileges/add" , [
		'as' => 'privileges-add', 'uses' => 'PrivilegesController@add', 'middleware' => ['permission:add privileges']
	]);
	Route::post("privileges/add" , [
		'as' => 'privileges-add-post', 'uses' => 'PrivilegesController@addPost', 'middleware' => ['permission:add privileges','csrf']
	]);
	Route::post("privileges/delete/{status}/{key}" , [
		'as' => 'privileges-delete', 'uses' => 'PrivilegesController@delete', 'middleware' => ['permission:delete privileges']
	]);
	Route::get("privileges/edit/{status}/{id}" , [
		'as' => 'privileges-edit', 'uses' => 'PrivilegesController@edit', 'middleware' => ['permission:edit privileges']
	]);
	Route::post("privileges/edit/{status}/{id}" , [
		'as' => 'privileges-edit-post', 'uses' => 'PrivilegesController@editPost', 'middleware' => ['permission:edit privileges','csrf']
	]);
	Route::get("privileges/{status}/{key?}" , [
		'as' => 'privileges', 'uses' => 'PrivilegesController@index', 'middleware' => ['permission:view privileges']
	]);

	/*Permission*/
	Route::get("permission/add" , [
		'as' => 'permission-add', 'uses' => 'PermisionController@add', 'middleware' => ['permission:add permission']
	]);
	Route::post("permission/add" , [
		'as' => 'permission-add-post', 'uses' => 'PermisionController@addPost', 'middleware' => ['permission:add permission','csrf']
	]);
	Route::get("permission/edit/{id}" , [
		'as' => 'permission-edit', 'uses' => 'PermisionController@edit', 'middleware' => ['permission:edit permission']
	]);
	Route::post("permission/edit/{id}" , [
		'as' => 'permission-edit-post', 'uses' => 'PermisionController@editPost', 'middleware' => ['permission:edit permission','csrf']
	]);
	Route::post("permission/delete/{key}" , [
		'as' => 'permission-delete', 'uses' => 'PermisionController@delete', 'middleware' => ['permission:delete permission']
	]);
	Route::get("permission/{key?}" , [
		'as' => 'permission', 'uses' => 'PermisionController@index', 'middleware' => ['permission:view permission']
	]);

	/*FILE MANAGER*/
	Route::get("file-manager/get/{type}/{page}/{order}/{key?}" , [
		'as' => 'file-manager-get', 'uses' => 'FilemanagerController@get'
	]);
	Route::get("file-manager/go" , [
		'as' => 'file-manager-go', 'uses' => 'FilemanagerController@go'
	]);
	Route::post("file-manager/store" , [
		'as' => 'file-manager-store', 'uses' => 'FilemanagerController@store'
	]);
	Route::get("file-manager/edit" , [
		'as' => 'file-manager-edit', 'uses' => 'FilemanagerController@edit'
	]);
	Route::get("file-manager/add" , [
		'as' => 'file-manager-add', 'uses' => 'FilemanagerController@add'
	]);
	Route::post("file-manager/update/{id}" , [
		'as' => 'file-manager-update', 'uses' => 'FilemanagerController@update'
	]);
});

Route::get('logout', [
	'as' => 'logout', 'uses' => 'Auth\LoginController@logout'
]);

Auth::routes();

//Route::get('/{url}', 'HomeController@notFound');