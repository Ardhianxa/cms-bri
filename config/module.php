<?php
return [
    'modules'=>[
        'admin',
        'web',
        'api',
        'Inbox',
        'Channel',
        'Article',
    ]
];