<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';
    protected $primaryKey = 'id';

    protected $fillable = [
        'parent_id',
		'menu',
		'path',
		'type',
        'status'
    ];

    protected $hidden = [
    ];

    public static function Data()
    {
        if(config('constant.CACHE')){
            $cache_name = "menu";
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::Data_data(), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::Data_data();
        }

        return $data;
    }

    public static function Data_data()
    {
    	$menu = Menu::select('id','parent_id','menu','level')->where('status',1)->where('level',1)->orderBy('menu','asc')->get();
        $menu->transform(function($menu){
            $menu->child = Menu::select('id','parent_id','menu','level')->where('status',1)->where('parent_id',$menu->id)->where('level',2)->orderBy('menu','asc')->get();
            $menu2 = $menu->child;
            $menu2->transform(function($menu2){
                $menu2->child = Menu::select('id','parent_id','menu','level')->where('status',1)->where('parent_id',$menu2->id)->where('level',3)->orderBy('menu','asc')->get();;
                return $menu2;
            });
            return $menu;
        });

        return $menu;
    }
}
