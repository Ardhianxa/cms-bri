<?php

namespace App\Http\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\Cache;
use Spatie\Permission\Models\Permission;

class M_Permission extends Model
{
    protected $table = 'permissions';
    protected $primaryKey = 'id';

    protected $fillable = [
    ];

    protected $hidden = [
    ];

    public static function Data($key,$page)
    {
        if(config('constant.CACHE')){
            $cache_name = "permission_data_".$key."_".$page;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::Data_data($key), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::Data_data($key);
        }

        return $data;
    }

    public static function Data_data($key)
    {
        $data = Permission::where('id','!=',0);

        if($key != null){
            $data->whereRaw("
            (
                name LIKE '%".$key."%' or
                group_name LIKE '%".$key."%'
            )
            ");
        }

        return $data->paginate(config('constant.LIMIT_DATA_TABLE'));
    }

    public function Child()
    {
        return $this->hasMany('App\Http\Models\M_Permission', 'group_name', 'group_name')
            ->select(['id', 'group_name', 'name', 'guard_name']);
    }
}
