<?php

namespace App\Http\Models; 

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\Cache;

class Users extends Model
{
    protected $table = 'users';

    protected $fillable = [
        'name',
		'email',
        'email_verified_at',
        'password',
        'remember_token'
    ];

    protected $hidden = [
    ];

    public static function Data($status)
    {
        if(config('constant.CACHE')){
            $cache_name = "users_data_".$status;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::Data_data($status), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::Data_data($status);
        }

        return $data;
    }

    public static function Detail($id)
    {
        if(config('constant.CACHE')){
            $cache_name = "users_detail_".$id;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::Detail_data($id), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::Detail_data($id);
        }

        return $data;
    }

    public static function Detail_data($id)
    {
         return Users::select('id','email')
         ->with('Users_privileges')
         ->where('id',$id)->first();
    }

    public function Users_privileges()
    {
        return $this->hasOne('App\Http\Models\UsersPrivileges', 'user_id', 'id')
            ->select(['id', 'user_id', 'privileges_id']);
    }

    public function Users_info()
    {
        return $this->hasOne('App\Http\Models\UsersInfo', 'user_id', 'id')
            ->select(['id', 'user_id', 'status', 'first_name','last_name']);
    }

    // ->with(['Users_info' => function($query) use ($status_code) {
    //     //$query->where('status', $status_code);
    // }]);

    public static function Data_data($status)
    {
        if($status=='active'){
            $status_code = 1;
        }
        elseif($status=='inactive'){
            $status_code = 0;
        }
        elseif($status=='pending'){
            $status_code = 2;
        }
        
        $data = Self::select('email','id','created_at')->with('Users_info');

        $result = $data->get()->filter(function($item) use ($status_code) {
            return $item->Users_info->status === $status_code;
        })->values();

        return $result;
    }
}
