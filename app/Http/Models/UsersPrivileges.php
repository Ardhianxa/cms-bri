<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\Cache;

class UsersPrivileges extends Model
{
    protected $table = 'users_privileges';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
		'privileges_id',
        'status'
    ];

    protected $hidden = [
    ];

    public static function Detail($id)
    {
        if(config('constant.CACHE')){
            $cache_name = "privileges_detail_".$id;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::Detail_data($id), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::Detail_data($id);
        }

        return $data;
    }

    public static function Detail_data($id)
    {
    	return UsersPrivileges::select('id')->where('user_id',$id)->first();
    }

    public function Privileges()
    {
        return $this->hasMany('App\Http\Models\Privileges', 'id', json_decode('privileges_id'))
            ->select(['id', 'name']);
    }
}
