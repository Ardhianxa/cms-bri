<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;
use Illuminate\Pagination\Paginator;
use Auth;

class FileManager extends Model
{
    protected $table = 'file_manager';
    protected $primaryKey = 'id';

    protected $fillable = [
    ];

    protected $hidden = [
    ];

    public static function Data($key,$type,$order,$page)
    {
        if(config('constant.CACHE')){
            $cache_name = "file-manager-".$key."-".$type."-".$order."-".$page;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::Data_data($key,$type,$order,$page), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::Data_data($key,$type,$order,$page);
        }

        return $data;
    }

    public static function edit($id)
    {
        if(config('constant.CACHE')){
            $cache_name = "file-manager-edit-ajax".$id;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::edit_data($id), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::edit_data($id);
        }

        return $data;
    }

    public static function edit_data($id)
    {
        return Self::select('id','title','caption','note','filename','share')->where('status',1)->where('id',$id)->first();
    }

    public static function Type($type)
    {
        if($type=='image'){
            $result = 1;
        }
        elseif($type=='video'){
            $result = 2;
        }
        elseif($type=='others'){
            $result = 3;
        }

        return $result;
    }

    public static function Data_data($key,$type,$order,$page)
    {
        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $type = Self::Type($type);

        $data = Self::select('id','title','caption','note','filename','valid_until','created_at','created_id')->where('status',1)->where('type',$type)
        ->whereRaw("
        (
            created_id = '".Auth::id()."' or 
            share = 1
        )
        ")
        ;

        if($key != null){
            $data->whereRaw("
            (
                title LIKE '%".$key."%' or 
                caption LIKE '%".$key."%'
            )
            ");
        }
        
        $result = $data->orderBy('created_at',$order)->paginate(8);

        return $result;
    }
}
