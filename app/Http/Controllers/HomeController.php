<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{ 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $title = 'HOME';
        $page = 'index';
        return view('home',compact('title','page'));
    }

    public function notFound(Request $request)
    {
        $page = '404';
        return view('handler',compact('page'));
    }
}
