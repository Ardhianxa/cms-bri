<?php

namespace App\Http\Controllers; 

use Illuminate\Http\Request;
use App\Http\Models\M_Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class PermisionController extends Controller 
{ 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request, $key = null)
    {
        $title = 'Permission';
        $page = 'permission';
        $page_number = $request->page;
        $active = M_Permission::Data($key,$page_number);
        
        return view($page,compact('title','page','active','key'));
    }

    public function add(Request $request)
    {
        $title = 'Add Permission';
        $page = 'add_permission';
        return view($page,compact('title','page'));
    }

    public function addPost(Request $request)
    {
        $find = M_Permission::select('id')->where('name',$request->name)->first();
        if(!isset($find->id)){
            $permission = Permission::create(['name' => $request->name, 'group_name' => $request->group_name]);
            $result = redirect()->route('permission')->with('flash_success', 'Successfully');
        }
        else{
            $result = redirect()->route('permission-add')->with('flash_failed', 'Duplicate');
        }
        
        return $result;
    }

    public function edit(Request $request)
    {
        $title = 'Edit Permission';
        $page = 'edit_permission';
        $permission = Permission::where('id',$request->id)->first();
        $role = Role::where('status',1)->get();

        return view($page,compact('title','page','permission','role'));
    }

    public function editPost(Request $request)
    {
        $find = M_Permission::select('id','name')->where('name',$request->name)->where('id','!=',$request->id)->first();
        if(!isset($find->id)){
            $old_permission = M_Permission::where('id',$request->id)->first();
            $old_permission->name = $request->name;
            $old_permission->group_name = $request->group_name;
            $old_permission->save();

            $result = redirect()->route('permission-edit',['id' => $request->id])->with('flash_success', 'Successfully');
        }
        else{
            $result = redirect()->route('permission-edit',['id' => $request->id])->with('flash_failed', 'Duplicate');
        }

        return $result;
    }

    public function delete(Request $request, $id)
    {
        $permission = M_Permission::where('id',$id)->delete();
        $request->session()->flash('flash_success', 'Successfully');
        echo route('permission');
    }
}
