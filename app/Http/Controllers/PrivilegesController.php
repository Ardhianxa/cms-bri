<?php

namespace App\Http\Controllers; 

use Illuminate\Http\Request;
use App\Http\Models\Privileges;
use App\Http\Models\Roles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Models\M_Permission;
use App\User;

class PrivilegesController extends Controller 
{ 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request, $status, $key = null)
    {
        $title = 'Privileges';
        $page = 'privileges';
        $active = Privileges::Data($status,'active',1,$key);
        $inactive = Privileges::Data($status,'inactive',0,$key);
       
        return view($page,compact('title','page','active','inactive','key','status'));
    }

    public function add(Request $request)
    {
        $title = 'Add Privileges';
        $page = 'add_privileges';
        $permission = M_Permission::select('group_name')->with('Child')->groupBy('group_name')->get();

        return view($page,compact('title','page','permission'));
    }

    public function edit(Request $request)
    {
        $title = 'Edit Privileges';
        $page = 'edit_privileges';
        $status = $request->status;
        $permission = M_Permission::select('group_name')->with('Child')->groupBy('group_name')->get();
        $role = Role::select('id','name','guard_name')->where('id',$request->id)->first();
        
        return view($page,compact('title','page','permission','role','status'));
    }

    public function editPost(Request $request)
    {
        //check duplicate
        $duplicate = Role::select('id')->where('name',$request->name)->where('id','!=',$request->id)->first();
        if(isset($duplicate->id)){
            return redirect()->route('privileges-edit',['status' => 'active', 'id' => $request->id])->with('flash_failed', 'Duplicate');
        }

        $role = Role::select('id','name','guard_name')->where('id',$request->id)->first();
        $role->name = $request->name;
        $role->save();

        foreach(Permission::get() as $permission_key => $permission_value){
            $role->revokePermissionTo($permission_value->name);
        }

        foreach($request->privileges as $privileges_key => $privileges_name){
            $role->givePermissionTo($privileges_name);
        }

        return redirect()->route('privileges',['status' => 'active'])->with('flash_success', 'Successfully');
    }

    public function addPost(Request $request)
    {
        $find = Roles::select('id')->where('name',$request->name)->first();
        if(!isset($find->id)){
            $role = Role::create(['name' => $request->name]);
            foreach($request->privileges as $privileges_key => $privileges_name){
                $role->givePermissionTo($privileges_name);
            }
            $result = redirect()->route('privileges',['status' => 'active'])->with("flash_success", "Successfully");
        }
        else{
            $result = redirect()->route('privileges-add')->with("flash_failed", "Duplicate");
        }
        
        return $result;
    }

    public function delete(Request $request, $status, $id)
    {
        $role = Role::select('id','name','guard_name','note')->where('id',$id)->first();

        if($status=='inactive'){
            $change = 1;
            
            if(!empty($role->note))
            {
                if(count(json_decode($role->note)) > 0){
                    foreach(json_decode($role->note) as $user_key => $user_value){
                        $user = User::where('id',$user_value)->first();
                        $user->assignRole($role->name);
                    }
                }
            }
        }
        else{
            $change = 0;
            $users = User::role($role->name)->get();
            $users_with_role = array();
            foreach($users as $users_key => $users_value){
                $users_with_role[] = $users_value->id;
                $user = User::where('id',$users_value->id)->first();
                $user->removeRole($role->name);
            }
            if(count($users_with_role) > 0){
                $role->note = json_encode($users_with_role);
            }
        }
        $role->status = $change;
        $role->save();
        $request->session()->flash('flash_success', 'Successfully');
        echo route('privileges',['status' => $status, 'key' => '']);
    }
}
