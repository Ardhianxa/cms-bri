<?php

namespace App\Http\Controllers;   

use Illuminate\Http\Request;
use Auth;
use App\Http\Models\UsersInfo;
use App\Http\Helpers\Helpers;
use App\Http\Models\ActivityUsers;
use App\User;
use DB;
use Image;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Redis;

class ProfileController extends Controller
{ 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    use HasRoles;

    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function activities(Request $request, $start, $end)
    {
        $title = 'ACTIVITIES';
        $page = 'activities';

        if (!validateDate($start,'d-m-Y') || !validateDate($end,'d-m-Y')) {
            return view('errors/404',compact('title','page'));
        }
        else{
            $start_db = date('Y-m-d',strtotime($start));
            $end_db = date('Y-m-d',strtotime($end));

            // $data = Redis::set('nama', 'Wisnu');
            // $get = Redis::get('nama');

            // $activities = Cache::remember("user_all", 10 * 60, function () use ($start_db,$end_db) {
            //     $activities = DB::connection('mysql')->select(DB::raw("
            //     SELECT left(ua.created_at,10) as tanggal
            //     FROM users_activity ua
            //     JOIN activity a ON a.id=ua.activity_id 
            //     WHERE left(ua.created_at,10) >= '$start_db' AND left(ua.created_at,10) <= '$end_db' AND a.status=1 AND ua.status=1 AND ua.user_id='".Auth::id()."'
            //     GROUP BY left(created_at,10)
            //     "));

            //     $activities = Self::addObject($activities,array('tanggal'));

            //     return $activities;
            // }); 

            $activities = DB::connection('mysql')->table('users_activity')->select([  
            DB::raw('left(users_activity.created_at,10) as tanggal'), 
            ])->join('activity','activity.id','=','users_activity.activity_id')
            ->where('activity.status', 1)
            ->where('users_activity.user_id', Auth::id())
            ->whereRaw("
            left(users_activity.created_at,10) >= '$start_db' and left(users_activity.created_at,10) <= '$end_db'
            ")
            ->groupBy(DB::raw('left(users_activity.created_at,10)'))
            ->orderBy('users_activity.created_at','desc')
            ->get();

            $activities->transform(function($activities) {
                $activities->activity = ActivityUsers::select('name','activity','content_id','users_activity.created_at','icon')->join('activity','activity.id','=','users_activity.activity_id')->where('activity.status',1)->where('users_activity.status',1)->where('users_activity.user_id',Auth::id())
                    ->where(DB::raw('left(users_activity.created_at,10)'),$activities->tanggal)->orderBy('users_activity.created_at','desc')
                    ->get();

                return $activities;
            });

            return view($page,compact('title','page','activities','start','end'));
        }
    }

    public function profile(Request $request)
    {
        //REDIS
        // $save = Self::setToRedis('abc', json_encode(array('ardi','ansah'), true), 60);
        // $response = Self::getFromRedis("abc");
        // print_r($response);
        // exit;
        // $curl = curl_init();
        // curl_setopt_array($curl, array(
        //     CURLOPT_URL => "https://panel.rapiwha.com/send_message.php?apikey=2RWI8PXRIOJBGAVW8WEU&number=6282213979929&text=testa",
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => "",
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 30,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => "GET",
        //   ));
          
        //   $response = curl_exec($curl);
        //   $err = curl_error($curl);
          
        //   curl_close($curl);
          
        //   if ($err) {
        //     echo "cURL Error #:" . $err;
        //   } else {
        //     echo $response;
        //   }

        // $url = "https://messages-sandbox.nexmo.com/v0.1/messages";
        // $params = ["to" => ["type" => "whatsapp", "number" => '087888111801'],
        //     "from" => ["type" => "whatsapp", "number" => "14157386170"],
        //     "message" => [
        //         "content" => [
        //             "type" => "text",
        //             "text" => "Hello from Vonage and Laravel :) Please reply to this message with a number between 1 and 100"
        //         ]
        //     ]
        // ];
        // $headers = ["Authorization" => "Basic " . base64_encode('d7c51ffc' . ":" . '1g1S7r6iPDZIeF6i')];

        // $client = new \GuzzleHttp\Client();
        // $response = $client->request('POST', $url, ["headers" => $headers, "json" => $params]);
        // $data = $response->getBody();


        /*watermark*/
        // $waterMarkPath = config('constant.ACTUAL_PATH').'public/assets/backend/img/watermark2.png';
        // $image = Image::make(config('constant.ACTUAL_PATH').'public/assets/backend/uploads/original/63b37d6a2896949fa74efad5e493019f.jpg');
        // $image->insert($waterMarkPath, 'bottom-right', 5, 5);
        // $image->save(config('constant.ACTUAL_PATH').'public/assets/backend/uploads/original/test3-watermark.jpg');

        // $user = User::where('id',1)->first();
        // $user->assignRole('superadmin');

        //$role = Role::where('id',3)->first();
        //$permission = Permission::create(['name' => 'view album']);
        // $permission = Permission::create(['name' => 'add permission']);
        // $permission = Permission::create(['name' => 'edit permission']);
        // $permission = Permission::create(['name' => 'delete permission']);
        //$role->givePermissionTo($permission);
        
        // $permission = Permission::create(['name' => 'add users']);
        // $role->givePermissionTo($permission);

        // $permission = Permission::create(['name' => 'edit users']);
        // $role->givePermissionTo($permission);

        // $permission = Permission::create(['name' => 'delete users']);
        // $role->givePermissionTo($permission);

        // $permission = Permission::create(['name' => 'view privileges']);
        // $role->givePermissionTo($permission);

        // $permission = Permission::create(['name' => 'add privileges']);
        // $role->givePermissionTo($permission);

        // $permission = Permission::create(['name' => 'edit privileges']);
        // $role->givePermissionTo($permission);

        // $permission = Permission::create(['name' => 'delete privileges']);
        // $role->givePermissionTo($permission);

        // $permission = Permission::create(['name' => 'view channel']);
        // $role->givePermissionTo($permission);

        // $permission = Permission::create(['name' => 'add channel']);
        // $role->givePermissionTo($permission);

        // $permission = Permission::create(['name' => 'edit channel']);
        // $role->givePermissionTo($permission);

        // $permission = Permission::create(['name' => 'delete channel']);
        // $role->givePermissionTo($permission);

        // $permission = Permission::create(['name' => 'view article']);
        // $role->givePermissionTo($permission);

        // $permission = Permission::create(['name' => 'add article']);
        // $role->givePermissionTo($permission);

        // $permission = Permission::create(['name' => 'edit article']);
        // $role->givePermissionTo($permission);

        // $permission = Permission::create(['name' => 'delete article']);
        // $role->givePermissionTo($permission);

        
        //$permission = Permission::where('id',9)->first();
        //$role->givePermissionTo($permission);
        // if(auth()->user()->can($permission)){
        //     $role->givePermissionTo($permission);
        // }

        // $update = UsersInfo::get()->pluck('first_name');
        // $return = $update->filter()->take(1);
        // return response()->json($return);
        
        $title = 'PROFILE';
        $page = 'profile';

        //get privileges name
        foreach(json_decode($request->user->getRoleNames()) as $value){
            $privileges[] = $value;
        }

        return view($page,compact('title','page','privileges'));
    }

    public function profileUpdate(Request $request)
    {
        $validate = $this->validate($request, [
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => 'required',
            'handphone' => 'required'
        ]);

        $update = UsersInfo::select('id')->where('user_id',Auth::id())->first();
        if(!isset($update)){
            return redirect()->back()->with('flash_failed', 'Account Not Found!');
        }

        $update->first_name = $request->first_name;
        $update->last_name = $request->last_name;
        $update->handphone = $request->handphone;

        /*password*/
        $password = $request->password;
        $repassword = $request->repassword;
        if($password != '' || $repassword != ''){
            if($password != $repassword){
                return redirect()->back()->with('flash_failed', 'Password Not Match');
            }
            else{
                $user = User::select('id','password')->where('id',Auth::id())->first();
                $user->password = bcrypt($password);
                $user->save();
            }
        }

        /*image from file manager*/
        if($request->config != ""){
            $container_image = Helpers::get_container_image($request->config);
            $image = Helpers::file_manager_image($request->$container_image,$request);
            if($image != ""){
                if($image != 'error'){
                    $update->image = $image;
                }
            }
        }
        /*end file manager*/

        $update->save();
        return redirect()->route('logout')->with('flash_success', 'Please log in again');
    }
}
