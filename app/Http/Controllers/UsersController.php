<?php 

namespace App\Http\Controllers;   

use Illuminate\Http\Request;
use App\Http\Models\Users;
use App\User;
use App\Http\Models\UsersInfo;
//use App\Http\Models\Mongo_Test;
use Mail;
//use DB;
use Spatie\Permission\Models\Role;

class UsersController extends Controller 
{ 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request, $status, $key = null)
    {
        // $data = Mongo_Test::whereRaw([
        //     'harga' => ['$gte' => 123]
        // ])->get();

        //$data = Mongo_Test::select('_id','judul','items')->where('items.product_id',4)->where('_id','5f2a6249aa1cd64a40003367')->orderBy('judul','desc')->first();

        // print_r($data);
        // exit;

        // $master_embed = array();

        // $embed_satuan = array();
        // $embed_satuan['product_id'] = 3;
        // $embed_satuan['price'] = 3000;
        // $embed_satuan['quantity'] = 50;
        // $master_embed[] = $embed_satuan;

        // $embed_satuan = array();
        // $embed_satuan['product_id'] = 4;
        // $embed_satuan['price'] = 4000;
        // $embed_satuan['quantity'] = 40;
        // $master_embed[] = $embed_satuan;

        // $mongo_insert = new Mongo_Test();
        // $mongo_insert->total = 900;
        // $mongo_insert->items = $master_embed;
        // $mongo_insert->save();

        // print_r($data);
        // exit;

        // $data = Redis::set('nama', 'Wisnu');
        // $get = Redis::get('nama');

        // $cache_name = "dfa";
        // if(Cache::has($cache_name)) {
        //     $value = Cache::get($cache_name);
        // }
        // else{
        //     $activities = DB::connection('mysql')->select(DB::raw("
        //     SELECT id
        //     FROM users_activity order by created_at desc limit 5 
        //     "));
        //     Cache::store('redis')->put($cache_name, 'baz', 10); //spesifik cache
        //     Cache::put($cache_name, $activities, now()->addMinutes(10));
        //     Cache::add($cache_name, $activities, now()->addMinutes(10));
        //     Cache::forever($cache_name, $activities); // simpan tanpa batas waktu
        //     Cache::forget($cache_name); // hapus cache
        //     $Cache::pull($cache_name); //hapus cache
        //     Cache::flush(); // hapus semua cache

        //     $value = Cache::remember($cache_name, now()->addMinutes(10), function () {
        //         return DB::connection('mysql')->select(DB::raw("
        //         SELECT id
        //         FROM users_activity order by created_at desc limit 5 
        //         "));
        //     }); //ga perlu get lagi

        //     $value = Cache::rememberForever($cache_name, function() {
        //         return DB::connection('mysql')->select(DB::raw("
        //         SELECT id
        //         FROM users_activity order by created_at desc limit 5 
        //         ")); //ga perlu get lagi
        //     });

        //     $value = Cache::get($cache_name);
        // }

        // print_r($value);
        // exit;

        $title = 'Users';
        $page = 'users';

        return view($page,compact('title','page','key','status'));
    }

    public function ajax(Request $request)
    {
        $data = Users::Data($request->status2);
        $role = Role::where('status',1)->get();
        $status2 = $request->status2;
        return view('users_ajax',compact('data','status2','role'));
    }

    public function add(Request $request)
    {
        $title = 'Add User';
        $page = 'add_users';
        $role = Role::where('status',1)->get();

        return view($page,compact('title','page','role'));
    }

    public function edit(Request $request)
    {
        $title = 'Edit User';
        $page = 'edit_users';
        $status = $request->status;
        $role = Role::where('status',1)->get();
        $user = User::where('id',$request->id)->first();

        if(!isset($user->id)){
            return view('errors.404');
        }

        return view($page,compact('title','page','role','user','status'));
    }

    public function editPost(Request $request)
    {
        $email = $request->email;
        $privileges = $request->privileges;

        $role = Role::where('status',1)->get();
        $user = User::where('id',$request->id)->first();

        /*remove all role*/
        foreach($role as $value){
            $user->removeRole($value->name);
        }

        /*save new role*/
        if(!empty($privileges) > 0){
            foreach($privileges as $privileges_key => $privileges_value){
                $user->assignRole($privileges_value);
            }
        }

        return redirect()->route('users-edit-post', ['status' => $request->status, 'id' => $request->id])->with('flash_success', 'Successfully');
    }

    public function activationPost(Request $request)
    {
        if($request->password != $request->password_confirmation)
        {
            return redirect()->route('users-activation',['token' => $request->token])->with('flash_failed', 'Password Tidak Sama');
        }
        else
        {
            if(strlen($request->password) < 6)
            {
                return redirect()->route('users-activation',['token' => $request->token])->with('flash_failed', 'Password Minimal 6 Karakter');
            }
            else
            {
                $users = Users::select('id')->where('remember_token',$request->token)->first();

                $usersInfo = UsersInfo::where('user_id',$users->id)->first();
                $usersInfo->first_name = $request->first_name;
                $usersInfo->last_name = $request->last_name;
                $usersInfo->handphone = $request->handphone;
                $usersInfo->status = 1;

                if($usersInfo->save()){
                    
                    $users->remember_token = null;
                    $users->password = bcrypt($request->password);
                    $users->save();
                    
                    return redirect()->route('login')->with('flash_success', 'Silahkan Login Dengan Password Baru Anda');
                }
                else{
                    return redirect()->route('users-activation',['token' => $request->token])->with('flash_failed', 'Internal Server Error 3');
                }
            }
        }
    }

    public function activation(Request $request)
    {
        $cekToken = Users::select('email')->join('users_info','users_info.user_id','=','users.id')->where('users_info.status',2)->where('remember_token',$request->token)->where('remember_token',$request->token)->first();
        if(!isset($cekToken)){
            return view('errors.404');
        }
        else{
            return view('auth.activation')->with(
                [
                    'token' => $request->token,
                    'email' => $cekToken->email
                ]
            );
        }
    }

    public function addPost(Request $request)
    {
        $email = $request->email;
        $privileges = $request->privileges;

        //cek duplicate
        $duplicate = Users::select('id')->where('email',$email)->first();
        if(isset($duplicate->id)){
            return redirect()->route('users-add')->with('flash_failed', 'Duplicate Data');
        }
        $remember_token = md5(date('Y-m-d H:i:s'));

        //sendmail
        $data = array(
            'token' => $remember_token
        );

        try{
            $send = Mail::send('emails.add_user', $data, function ($message) use ($request)
            {
                $message->from(config('constant.MAIL_FROM'), config('constant.APP_NAME'));
                $message->to($request->email)->subject('CMS Activation Account');
            });

            //save user
            $saveUser = new User();
            $saveUser->name = "";
            $saveUser->password = "";
            $saveUser->email = $email;
            $saveUser->remember_token = $remember_token;
            if($saveUser->save()){
    
                //save users info
                $usersInfo = new UsersInfo();
                $usersInfo->user_id = $saveUser->id;
                $usersInfo->status = 2; //as draft
                if($usersInfo->save()){

                    /*SET ROLE*/
                    foreach($privileges as $privileges_key => $privileges_value){
                        $saveUser->assignRole($privileges_value);
                    }

                    return redirect()->route('users-add')->with('flash_success', 'Successfully');
                }
                else{
                    return redirect()->route('users-add')->with('flash_failed', 'Internal Server Error 2');
                }
            }
            else{
                return redirect()->route('users-add')->with('flash_failed', 'Internal Server Error');
            }
        }
        catch (Exception $e){
            
        }
    }

    public function delete(Request $request, $status, $id)
    {
        $delete = UsersInfo::select('id','status','first_name','last_name','handphone')->where('user_id',$id)->first();

        if($status=='active' || $status=='pending'){
            $change = 0;
        }
        else{
            if($delete->first_name=="" && $delete->last_name=="" && $delete->handphone==""){
                $change = 2;
            }
            else{
                $change = 1;
            }
        }

        $delete->status = $change;
        $delete->save();
        $request->session()->flash('flash_success', 'Successfully');
        echo route('users',['status' => $status, 'key' => '']);
    }
}
