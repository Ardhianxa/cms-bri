<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Http\Models\Users;
use Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        $title = 'Forgot Password';
        $page = 'forgot_password';
        return view('auth.passwords.email',compact('title','page'));
    }

    public function sendResetLinkEmail(Request $request)
    {
        ini_set('max_execution_time', 300);
        $this->validateEmail($request);

        //cek email
        $cek = Users::select('id')->where('email',$request->email)->first();
        if(!isset($cek->id)){
            return redirect()->route('password.request')->with('flash_failed', 'Email Not Found!');
        }

        //update token
        $data['token'] = md5(date('Y-m-d H:i:s'));
        $cek->remember_token = $data['token'];
        if($cek->save()){

            try{
                $send = Mail::send('emails.password', $data, function ($message) use ($request)
                {
                    $message->from(config('constant.MAIL_FROM'), config('constant.APP_NAME'));
                    $message->to($request->email)->subject('CMS Recovery Password');
                });

                return redirect()->route('password.request')->with('flash_success', 'A Recovery Link Has Been Sent To Your Email');
            }
            catch(Exception $e){
                return redirect()->route('password.request')->with('flash_failed', 'Internal Server Error !');
            }
        }
        else{
            return redirect()->route('password.request')->with('flash_failed', 'Internal Server Error !');
        }
    }
}
