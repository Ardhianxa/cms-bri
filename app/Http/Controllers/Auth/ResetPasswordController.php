<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Http\Models\Users;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request, $token = null)
    {
        //cek token
        $cekToken = Users::where('remember_token',$token)->first();
        if(!isset($cekToken)){
            return view('errors.404');
        }
        else{
            return view('auth.passwords.reset')->with(
                ['token' => $token]
            );
        }
    }

    public function reset(Request $request)
    {
        $password = $request->password;
        $token = $request->token;
        $password_confirmation = $request->password_confirmation;

        $cekToken = Users::where('remember_token',$token)->first();
        if($password != $password_confirmation)
        {
            return redirect()->route('password.reset', ['reset' => $token])->with('flash_failed', 'Password Tidak Sama');
        }
        else
        {
            if(strlen($password)<6)
            {
                return redirect()->route('password.reset', ['reset' => $token])->with('flash_failed', 'Password Minimal 6 Karakter');
            }
            else
            {
                $cekToken->remember_token = null;
                $cekToken->password = bcrypt($password);
                $cekToken->save();
                
                return redirect()->route('login')->with('flash_success', 'Silahkan Login Dengan Password Baru Anda');
            }
        }
    }
}
