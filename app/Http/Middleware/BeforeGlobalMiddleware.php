<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Helpers;
use App\User;

class BeforeGlobalMiddleware 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Helpers::beep($request);
        Helpers::message($request);

        if(Auth::id() != null){
            $request->user = User::where('id',Auth::id())->first();
        }

        return $next($request);
    }
}
