<?php  

namespace App\Http\Helpers;   

use App\Http\Models\Activity;
use App\Http\Models\ActivityUsers;
use App\Http\Models\Privileges; 
use Session;
use App\Modules\ConsumerLetter\Models\Consumer_Letter;
use App\Modules\Inbox\Models\Inbox;
use Auth;
use Illuminate\Support\Facades\Cache;
use Image;
use File;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Redis;

class Helpers
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    } 

    public static function check($results, $single = false)
    {
        if (!empty($results))
            if(is_array($results)){
                $check = count($results);
            }
            else{
                $check = count($results->toArray());
            }
        if (empty($check)) :
            return null;
        else :
            if (!$single) {
                if(is_array($results)){
                    $results = array_values($results);
                }
                else{
                    $results = array_values($results->toArray());
                }
                
            } else {
                $results = $results->toArray();
            }
        endif;
        return $results;
    }

    public static function prepareData($results)
    {
        if ($results) {
            $data['data'] = $results;
            $data['status']['code'] = 0;
            $data['status']['statusText'] = 'OK';
        } else {
            $data['data'] = [];
            $data['status']['code'] = 404;
            $data['status']['statusText'] = 'Not Found';
        }
        return $data;
    }

    public static function redisTTL()
    {
        return [
            "fastest" => 60,        // 1 menit
            "faster" => 120,        // 2 menit
            "fast" => 1800,         // 30 menit
            "slow" => 3600,         // 60 menit
            "slower" => 86400,      //  1 hari
            "slowest" => 259200,    //  3 hari
            "long" => 604800,       //  7 hari
            "longer" => 1209600,    //  14 hari
            "longest" => 2592000    //  30 hari
        ];
    }

    public static function getFromRedis($keys, $debug = false)
    {
        if ($debug)
            dd(Redis::get($keys));

        return Redis::get($keys);
    }
    
    public static function setToRedis($key, $value, $ttl = null)
    {
        if ($value) {
            if ($ttl == null) {
                $ttl = env('REDIS_TTL', 1) * 300;
            } else {
                $ttl = env('REDIS_TTL', 1) * $ttl;
            }
            try {
                app('redis')->set($key, $value);
                app('redis')->expireat($key, time() + $ttl);
            } catch (\Exception $e) {
                return null;
            }
        } else {
            return null;
        }
    }

    public static function Paginator($data, $total, $per_page, $currenct_page, $base_url)
    {
        $result = new LengthAwarePaginator
        (
            $data, 
            $total, 
            $per_page, 
            $currenct_page, 
            ['path' => $base_url]
        );
      
        return $result;
    }

    public static function watermark($destinationPath,$filename,$watermark_position)
    {
        /*bikin image watermark*/
        $watermark_path = config('constant.ACTUAL_PATH').'backend/img/new_watermark.png';
        list($width_pic, $height_pic) = getimagesize($destinationPath.$filename);
        list($width, $height) = getimagesize($watermark_path);
        $nw = 0.5 * $width_pic;
        $nh = ($nw * $height)/$width;
        $nh = round($nh,0);
        $img = Image::make(config('constant.ACTUAL_PATH').'backend/img/new_watermark.png');
        $img->resize($nw, $nh);
        $new_watermark = config('constant.ACTUAL_PATH').'backend/img/watermark_'.md5(date('Y-m-d H:i:s')).'.png';
        $img->save($new_watermark);

        $image = Image::make($destinationPath.$filename);
        $image->insert($new_watermark, $watermark_position, 5, 5);
        $image->save($destinationPath.$filename);

        if(File::exists($new_watermark)) {
            File::delete($new_watermark);
        }
    }

    public static function crop_position($result,$target_width,$target_height)
    {
        list($width, $height, $type, $attr) = getimagesize(config('constant.ACTUAL_PATH').'uploads/img/original/'.$result);
        $nw = $width;
        $nh = ($nw * $target_height)/$target_width;
        $nh = round($nh,0);
        $ny = ($height-$nh) / 2;
        $ny = 0; //jika diambil dari atas

        $result = array();
        $result['x'] = 0;
        $result['y'] = $ny;
        $result['w'] = $nw;
        $result['h'] = $nh;

        return $result;
    }

    public static function get_container_image($config)
    {
        return json_decode($config)->container_image;
    }

    public static function config_modified($config,array $params = array())
    {
        $config = json_decode($config);
        foreach($params as $params_key => $params_value)
        {
            $config->$params_key = $params_value;
        }
        return json_encode($config);
    }

    public static function file_manager_image($filename,$request)
    {
        $config = json_decode($request->config);
        if(!empty($config->auto_crop)){
            if($config->auto_crop->status){
                foreach($config->auto_crop->data as $key => $value){
                    $crop_position = Self::crop_position($filename,$value->width,$value->height);
                    $crop = Self::Crop(false,config('constant.ACTUAL_PATH').'uploads/img/original/'.$filename,config('constant.ACTUAL_PATH').$value->segment.'/',$filename,$crop_position['x'],$crop_position['y'],$crop_position['w'],$crop_position['h'],$value->width,$value->height);
                }
            }
        }

        if(!empty($config->crop)){
            if($config->crop->status){
                if($filename != "")
                {
                    $komponen = $request->komponen;
                    $komponenexplode = explode(",",$komponen);
                    $suksesCrop = 0;
                    for($komp = 0; $komp < count($komponenexplode); $komp++)
                    {
                        $container = $komponenexplode[$komp];
                        $container_replace = str_replace("garing","/",$container);
                        $crop_param = Self::crop_param($container);
                        $crop_param_x = $crop_param['x'];
                        $crop_param_y = $crop_param['y'];
                        $crop_param_w = $crop_param['w'];
                        $crop_param_h = $crop_param['h'];
                        $crop_param_width = $crop_param['width'];
                        $crop_param_height = $crop_param['height'];
                        $crop = Self::Crop(false,config('constant.ACTUAL_PATH').'uploads/img/original/'.$filename,config('constant.ACTUAL_PATH').$container_replace.'/',$filename,$request->$crop_param_x,$request->$crop_param_y,$request->$crop_param_w,$request->$crop_param_h,$request->$crop_param_width,$request->$crop_param_height);

                        if($crop){
                            $suksesCrop++;
                        }
                    }
                    if($suksesCrop==count($komponenexplode)){
                        $filename = $filename;
                    }
                    else{
                        $filename = "error";
                    }
                }
            }
        }

        return $filename;
    }

    public static function does_url_exists($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    
        if ($code == 200) {
            $status = true;
        } else {
            $status = false;
        }
        curl_close($ch);
        
        return $status;
    }

    public static function crop_param($container)
    {
        $result = array();
        $result['x'] = 'x_'.$container;
        $result['y'] = 'y_'.$container;
        $result['w'] = 'w_'.$container;
        $result['h'] = 'h_'.$container;
        $result['width'] = 'width_'.$container;
        $result['height'] = 'height_'.$container;

        return $result;
    }

    public static function Crop($watermark,$original,$dir_new,$new,$x,$y,$w,$h,$targ_w,$targ_h){ 
        if (!is_dir($dir_new)){
            mkdir($dir_new);  
            chmod($dir_new,0777);
        }
        $dst_r = ImageCreateTrueColor($w, $h);
        $what = getimagesize($original);
        switch(strtolower($what['mime'])) {
            
            case 'image/png':
                imagesavealpha($dst_r, TRUE);
                $empty = imagecolorallocatealpha($dst_r,0x00,0x00,0x00,127);
                imagefill($dst_r, 0, 0, $empty);
                $img_r = imagecreatefrompng($original);
                imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $w, $h, $w, $h);
                $q=9/100;
                $quality = 5;
                //$quality*=$q;
                imagepng($dst_r, $dir_new . $new, $quality);
                break;
           
            case 'image/jpeg':
                $img_r = imagecreatefromjpeg($original);
                imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $w, $h, $w, $h);
                imagejpeg($dst_r, $dir_new . $new, 50);
                break;
           
            case 'image/gif':
                $img_r = imagecreatefromgif($original);
                imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $w, $h, $w, $h);
                imagegif($dst_r, $dir_new . $new, 50);
                break;
    
            default: die();
        }

        //CREATE NEW IMAGE
        list($width, $height) = getimagesize($dir_new.$new);
        $nw = $targ_w;
        $nh = ($nw * $height)/$width;
        $nh = round($nh,0);
        $img = Image::make($dir_new.$new);
        $img->resize($nw, $nh);
        $img->save($dir_new.$new);

        return true;
    }

    public static function cacheRemove($request)
    {
        $routeName = $request->route()->getName();
        if($_POST)
        {
            $flush = Cache::flush();
        }
        return true;
    }

    public static function message($request)
    {
        if(config('constant.CACHE')){
            $cache_name = "message_data_".Auth::id();
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::message_data($request), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::message_data($request);
        }

        return $data;
    }

    public static function message_data($request)
    {
        $message = Inbox::select('inbox.id','inbox.updated_at','sender_id','first_name','last_name','image')
        ->join('users_info','users_info.user_id','=','inbox.sender_id')
        ->where('inbox.status',1)->where('received_id',Auth::id())->orderBy('inbox.updated_at','desc')->orderBy('baca','desc')->skip(0)->take(5)->get();

        if(isset($message[0]->id)){
            $request->message = $message;
        }

        return true;
    }

    public static function beep($request)
    {
        if(config('constant.CACHE')){
            $cache_name = "beep_data_".Auth::id();
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::beep_data($request), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::beep_data($request);
        }

        return $data;
    }

    public static function beep_data($request)
    {
        $message = Inbox::select('id')->where('baca',2)->where('received_id',Auth::id())->first();
        if(isset($message->id)){
            $request->beep = 'beep';
        }

        return true;
    }

    public static function privileges($menu_id,$act,$data = null)
    {
        $result = 0;
        if($data==null){
            $data = json_decode(Session::get('privileges_data'));
        }
        else{
            $data = json_decode($data);
        }

        if(empty($data)){
            return false;
        }
        else{
            if(count($data) == 0){
                return false;
            }
            else{
                if($act=='view-consumer-letter'){
                    $judul = Consumer_Letter::select('judul')->where('id',$menu_id)->first();
                    if(isset($judul->judul)){
                        $privileges_id = json_decode(Session::get('privileges_id'));
                        if(
                            $judul->judul==1 && in_array(2, $privileges_id) || 
                            $judul->judul==2 && in_array(5, $privileges_id) || 
                            $judul->judul==3 && in_array(6, $privileges_id)
                            ){
                            $result++;
                        }
                    }
                    else{
                        return false;
                    }
                }
                else{
                    foreach($data as $value){
                        if($value->menu_id==$menu_id && $value->$act==1){
                            $result++;
                        }
                    }
                }
    
                if($result > 0){
                    return true;
                }
    
                return false;
            }
        }
    }

    public static function create_privileges($privileges_id)
    {
        $privileges_data = array();
        $Privileges_data = Privileges::select('data')->where('status',1)->whereIn('id',json_decode($privileges_id))->get();
        foreach($Privileges_data as $Privileges_data_value){
            $Privileges_data_temp = json_decode($Privileges_data_value->data);
            foreach($Privileges_data_temp as $Privileges_data_temp_value){
                $privileges_data[] = $Privileges_data_temp_value;
            }
        }

        return $privileges_data;
    }

    public static function Image($file)
    {
        $basename = basename($file);
        if($basename=="users"){
            $images = config('constant.ASSETS_URL').'backend/img/avatar/avatar-1.png';
        }
        else{
            $file_headers = @get_headers($file);
            if(
                $file_headers[0] == 'HTTP/1.1 404 Not Found' || $file_headers[0] == 'HTTP/1.0 404 Not Found' || 
                ($file_headers[0] == 'HTTP/1.0 302 Found' && $file_headers[7] == 'HTTP/1.0 404 Not Found')
            ){
                $images = config('constant.ASSETS_URL').'backend/img/avatar/avatar-1.png';
            } 
            else{
                $images = $file;
            }
        }
        
        return $images; 
    }

    public static function activity($request)
    {
        if(!empty(Auth::id()) && $_POST){
            $data = array();
            $data['user_id'] = Auth::id();
            $data['url'] = $request->fullUrl();
            $data['data'] = $request->all();
            $savedata = json_encode($data).'<-->';
            $path = storage_path('logs/access/');

            //folder tahun
            if(!file_exists($path.date('Y').'/'))
            {
                //folder not exists
                mkdir($path.date('Y').'/');
                chmod($path.date('Y').'/',0755);
            }

            //folder bulan
            if(!file_exists($path.date('Y').'/'.date('m').'/'))
            {
                //folder not exists
                mkdir($path.date('Y').'/'.date('m').'/');
                chmod($path.date('Y').'/'.date('m').'/',0755);
            }

            if(!file_exists($path.date('Y').'/'.date('m').'/'.date('d').'.txt'))
            {
                $fp = fopen($path.date('Y').'/'.date('m')."/".date('d').".txt","wb");
                fwrite($fp,$savedata);
                fclose($fp);
                chmod($path.date('Y').'/'.date('m').'/'.date('d').'.txt',0755);
            }
            else
            {
                $insert = file_put_contents($path.date('Y').'/'.date('m').'/'.date('d').'.txt', $savedata.PHP_EOL , FILE_APPEND | LOCK_EX);
            }
        }

        return true;
    }

    public static function generateJwt($payload,$secret_jwt,$algorithm,$token,$sess_id,$chiper_aes)
    {
        $jwt = JWT::encode(json_decode($payload), $secret_jwt, $algorithm);
        $code = ssl_encrypt($jwt,$token,$sess_id,$chiper_aes);

        return $code;
    }

    public static function response($payload, $message, $secret_key, $sess_id)
    {
        $token_jwt_response = JWT::encode($payload, config('key.secret_jwt'), 'HS256');
        $ivlen = openssl_cipher_iv_length(config('key.chiper_aes'));
        $iv = openssl_random_pseudo_bytes($ivlen);
        $iv_base64 = base64_encode($iv);
        $ciphertext = openssl_encrypt($token_jwt_response, config('key.chiper_aes'), base64_decode($secret_key), $options=0, $iv, $tag, $sess_id);

        //single return
        if(isset($payload['code']) && $payload['code'] != 200){
            return response()->json([
                'txt' => $ciphertext.'.'.$iv_base64.'.'.base64_encode($tag),
                'code' => $payload['code'],
                'message' => $message
            ], $payload['code']); 
        } else {
            return response()->json([
                'txt' => $ciphertext.'.'.$iv_base64.'.'.base64_encode($tag),
                'code' => 200,
                'message' => $message
            ], 200);  
        }
    }
}
