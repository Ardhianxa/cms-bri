<?php
namespace App\Modules\Channel\Models; 

use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $table = 'channel';
    protected $primaryKey = 'id';

    protected $fillable = [
        'parent_id',
		'name',
        'status'
    ];

    protected $hidden = [
    ];

    public static function process($request)
    {
        if(!empty($request->id)){
            $update = Channel::select('id')->where('id',$request->id)->first();
            $update->name = $request->title;
            $update->slug = $request->slug;
            $update->meta_title = $request->meta_title;
            $update->meta_description = $request->meta_description;
            if($request->status==null){
                $request->status = 2;
            }
            $update->status = $request->status;
            $update->save();
        }
        else{
            $new = new Channel();
            $new->parent_id = $request->parent_id;
            $new->level = $request->level;
            $new->name = $request->title;
            $new->slug = $request->slug;
            $new->meta_title = $request->meta_title;
            $new->meta_description = $request->meta_description;
            $new->status = $request->status;
            $new->save();
        }

        return true;
    }

    public static function detail($id)
    {
        if(config('constant.CACHE')){
            $cache_name = "channel_detail_".$id;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::detail_data($id), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::detail_data($id);
        }

        return $data;
    }

    public static function detail_data($id)
    {
        return Self::select('name','meta_title','meta_description','status','slug')->where('id',$id)->first();
    }

    public static function data()
    {
        if(config('constant.CACHE')){
            $cache_name = "category_data";
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::data_data(), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::data_data();
        }

        return $data;
    }

    public static function data_data()
    {
        $data = Self::select('id','name','parent_id','status')->where('status','!=',0)->where('parent_id',0)->orderBy('name','asc')->get();
        $data->transform(function($data){
            $data->child = Self::select('id','name','parent_id','status')->where('status','!=',0)->where('parent_id',$data->id)->orderBy('name','asc')->get();
            $data->child->transform(function($data){
                $data->child = Self::select('id','name','parent_id','status')->where('status','!=',0)->where('parent_id',$data->id)->orderBy('name','asc')->get();
                $data->child->transform(function($data){
                    $data->child = Self::select('id','name','parent_id','status')->where('status','!=',0)->where('parent_id',$data->id)->orderBy('name','asc')->get();
                    return $data;
                });
                return $data;
            });
            return $data;
        });
        return $data;
    }
}
