<?php
 
namespace App\Modules\Channel\Controllers;

require (__DIR__.'/../Helpers/Fungsi.php'); //lebih prioritas helpers di master 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\Channel\Models\Channel;
use Helpers;

class ChannelController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    } 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {
        $title = 'Channel';
        $page = 'Channel::data';
        $menu_id = 10;
        $data = Channel::data();

        return view($page,compact('title','page','menu_id','data'));
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $data = Channel::detail($id);
        echo json_encode($data);
    }

    public function post(Request $request)
    {
        $process = Channel::process($request);
        return redirect()->route('channel')->with('flash_success', 'Successfully');
    }

    public function delete(Request $request, $id)
    {
        $delete = Channel::select('id')->where('id',$id)->first();
        $delete->status = 0;
        $delete->save();
        $request->session()->flash('flash_success', 'Successfully');
        echo route('channel');
    }
}
