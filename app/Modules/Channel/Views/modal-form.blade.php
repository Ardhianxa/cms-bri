<form method="POST" action="{{ route('channel-post') }}">
    <div class="modal fade modal-category-add" id="ads" tabindex="-1" role="dialog" aria-labelledby="modal-category" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="z-index:10000">
            <div class="modal-header">
                <h4 class="modal-title" id="" style="padding-bottom:20px;"><div class="category-title"></div></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                @csrf
                <div class="parent-text" align="center"></div>
                <div class="parent-id"></div>
                <div class="category-id"></div>
                <div class="category-level"></div>
                <div class="row">                               
                    <div class="form-group col-sm-12 col-md-12 col-12">
                        <label>Name</label>
                        <input type="text" class="form-control title" name="title" value="" required="required">
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-12">
                        <label>Slug</label>
                        <input type="text" class="form-control slug" name="slug" value="" required="required">
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-12">
                        <label>Meta Ttitle</label>
                        <textarea class="form-control meta_title" name="meta_title" rows="30" cols="10"></textarea>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-12">
                        <label>Meta Description</label>
                        <textarea class="form-control meta_description" name="meta_description" rows="30" cols="10"></textarea>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-12">
                        <div class="category-status"></div>
                    </div>

                    <div class="form-group col-sm-12 col-md-12 col-12" align="right">
                        <input type="submit" class="btn btn-md btn-primary" value="Submit">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</form>