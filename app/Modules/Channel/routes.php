<?php
Route::group(  
[
	'namespace' => 'App\Modules\Channel\Controllers', 
	'prefix' => '', 
	'middleware' => [
						'web',
						'auth',
						'session',
						'before_global',
						'after_global',
						'App\Modules\Channel\Middleware\BeforeMiddleware',
						'App\Modules\Channel\Middleware\AfterMiddleware'
					]
], 
function ()
{
	Route::get('channel', [
		'as' => 'channel', 'uses' => 'ChannelController@index', 'middleware' => ['permission:view channel']
	]);

	Route::get('channel/edit/{id}', [
		'as' => 'channel-edit', 'uses' => 'ChannelController@edit', 'middleware' => ['permission:edit channel']
	]);

	Route::post('channel/delete/{id}', [
		'as' => 'channel-delete', 'uses' => 'ChannelController@delete', 'middleware' => ['permission:delete channel']
	]);

	Route::post('channel/post', [
		'as' => 'channel-post', 'uses' => 'ChannelController@post', 'middleware' => ['permission:add channel']
	]);
});
?>