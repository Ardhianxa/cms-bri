@extends('layouts.app') 

@section('content')
<div class="col-12 col-md-12 col-lg-12">
    <div class="card">
        <form method="post" id="form-file-manager-data" class="needs-validation" enctype= multipart/form-data novalidate="" action="{{ route('article-edit-post',['id' => $data->id, 'status' => $status]) }}">
          @csrf
            <div class="card-body">
              	<div class="row">                               
                    <div class="form-group col-sm-12 col-md-12 col-lg-6"> 
                    	<label>Title</label>
                    	<input type="text" class="form-control" name="title" value="{{$data->title}}">
                    	<div class="invalid-feedback">
                      	Please fill in the email
                    	</div>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-6"> 
                    	<label>Channel</label>
                    	<select required name="channel_id" id="channel" class="form-control">
                            <option value="">-Choose-</option>
                            @foreach($channel as $channel_key => $channel_value)
                                <!--LEVEL 1-->
                                @if(count($channel_value->child)==0)
                                    <option @if($data->channel_id==$channel_value->id) selected="selected" @endif value="{{$channel_value->id}}">{{$channel_value->name}}</option>
                                @else
                                    <optgroup label="{{$channel_value->name}}">
                                        <!--LEVEL 2-->
                                        @foreach($channel_value->child as $channel_value2)
                                            @if(count($channel_value2->child)==0)
                                                <option @if($data->channel_id==$channel_value2->id) selected="selected" @endif value="{{$channel_value2->id}}">{{$channel_value2->name}}</option>
                                            @else
                                                <optgroup label="{{$channel_value2->name}}">
                                                    <!--LEVEL 3-->
                                                    @foreach($channel_value2->child as $channel_value3)
                                                        @if(count($channel_value3->child)==0)
                                                            <option @if($data->channel_id==$channel_value3->id) selected="selected" @endif value="{{$channel_value3->id}}">{{$channel_value3->name}}</option>
                                                        @else
                                                            <optgroup label="{{$channel_value3->name}}">
                                                                <!--LEVEL 4-->
                                                                @foreach($channel_value3->child as $channel_value4)
                                                                    <option @if($data->channel_id==$channel_value4->id) selected="selected" @endif value="{{$channel_value4->id}}">{{$channel_value4->name}}</option>
                                                                @endforeach
                                                            </optgroup>
                                                        @endif
                                                    @endforeach
                                                </optgroup>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                @endif
                                
                            @endforeach
                        </select>
                    	<div class="invalid-feedback">
                      	Please fill in the email
                    	</div>
                    </div>
                </div>
                <div class="row">                               
                    <div class="form-group col-sm-12 col-md-12 col-lg-6"> 
                    	<label>Sub Title</label>
                    	<input type="text" class="form-control" name="subtitle" value="{{$data->subtitle}}">
                    	<div class="invalid-feedback">
                      	Please fill in the email
                    	</div>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-6"> 
                    	<label>Publish Date</label>
                    	<input type="text" class="form-control datetimepicker" value="{{date('d-m-Y H:i',strtotime($data->publish_date))}}" name="publish_date">
                    	<div class="invalid-feedback">
                      	Please fill in the email
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">                               
                    <div class="form-group col-sm-12 col-md-12 col-lg-6"> 
                    	<label>Mete Keyword</label>
                    	<textarea class="form-control" rows="5" name="meta_keyword" cols="20" style="height:100% !important;">{{$data->meta_keyword}}</textarea>
                    	<div class="invalid-feedback">
                      	Please fill in the email
                    	</div>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-6"> 
                    <label>Mete Description</label>
                    	<textarea class="form-control" rows="5" name="meta_description" cols="20" style="height:100% !important;">{{$data->meta_description}}</textarea>
                    	<div class="invalid-feedback">
                      	Please fill in the email
                    	</div>
                    </div>
                </div>
                <br>
                <div class="row">                               
                    <div class="form-group col-sm-12 col-md-12 col-lg-6"> 
                    	<label>Description</label>
                    	<textarea class="form-control" rows="5" name="summary" cols="20" style="height:100% !important;">{{$data->summary}}</textarea>
                    	<div class="invalid-feedback">
                      	Please fill in the email
                    	</div>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-6"> 
                        <label>Set Headline</label><br>
                        <input type="checkbox" @if($data->is_headline==1) checked="checked" @endif name="is_headline" value="1" style="transform: scale(2);">
                    </div>
                </div>
                <div class="row">                               
                    <div class="form-group col-sm-12 col-md-12 col-lg-6"> 
                        <br>
                    	<label>Video</label>
                    	<input type="text" class="form-control" name="video" value="{{$data->video}}">
                    	<div class="invalid-feedback">
                      	Please fill in the email
                    	</div>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-6"> 
                        <br>
                    	<label>Tagging</label>
                    	<select class="js-example-tags form-control" name="tagging[]" id="tagging" multiple="multiple">
                        @if(count($data->tagging) > 0)
                            @foreach($data->tagging as $tagging)
                                <option selected="selected">{{$tagging->data->name}}</option>
                            @endforeach
                        @endif
                        </select>
                    	<div class="invalid-feedback">
                      	Please fill in the email
                    	</div>
                    </div>
                </div>
                <div class="row">                               
                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <label>Content</label>
                        <textarea class="summernote" name="body">{!! $data->body !!}</textarea>
                    </div>
                </div>
                <div class="row">                               
                    <div class="form-group col-sm-12 col-md-12 col-lg-2">
                        <label>Cover</label><br>
                        <div class="file-manager-data-default-image">
                            @if(!empty($data->cover->file_manager->filename))
                                <img src="{{Helpers::Image(config('constant.ASSETS_URL').'uploads/img/original/'.$data->cover->file_manager->filename)}}" width="200px">
                                <textarea style="margin-top:20px;width:100%;height:100px;" placeholder="Image Caption" name="custom_caption" id="custom_caption">{{ $data->cover->custom_caption }}</textarea>
                            @else
                                <img src="{{config('constant.ASSETS_URL')}}images/example-image.jpg" width="200px">
                            @endif
                        </div>
                        <div class="file-manager-data"></div><br>
                        <a href="#" onclick="file_manager_show('{{config('constant.CONFIG_IMAGE_ARTICLE_COVER')}}')" class="btn btn-md btn-primary">Select Image</a>
                        <a href="#" class="btn btn-md btn-danger" onclick="clear_box_image('{{config('constant.CONFIG_IMAGE_ARTICLE_COVER')}}')">Remove</a>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-1">
                        <label>Other Image</label><br>
                        <a class="btn btn-lg btn-success" onclick="article_add_other_image('{{route('article-add-other-image')}}','container-other-image')" style="font-size:20px;">+</a>
                    </div>
                    <div class="form-group col-sm-12 col-md-12 col-lg-9">
                        <div class="row container-other-image">
                            <label>&nbsp</label><br>
                            @if(count($data->gallery) > 0)
                                @foreach($data->gallery as $gallery_key => $gallery_value)
                                    @php
                                        $config_modified = Helpers::config_modified(config('constant.CONFIG_IMAGE_ARTICLE_COVER'),array('file_manager_id_name' => 'file_manager_id_'.md5($gallery_value->id), 'config_name' => 'config_'.md5($gallery_value->id), 'container_image' => 'image_data_'.md5($gallery_value->id), 'container' => 'file-manager-data-'.md5($gallery_value->id)));
                                    @endphp
                                    <div class="form-group col-sm-12 col-md-12 col-lg-3 {{md5($gallery_value->id)}}" style="">
                                        <label>&nbsp;</label><br>
                                        <div class="file-manager-data-{{md5($gallery_value->id)}}-default-image" style="display: none;">
                                            <img src="{{Helpers::Image(config('constant.ASSETS_URL').'uploads/img/original/'.$gallery_value->file_manager->filename)}}" width="200px">
                                            <textarea style="margin-top:20px;width:100%;height:100px;" placeholder="Image Caption" name="custom_caption_{{md5($gallery_value->id)}}" id="custom_caption">{{ $gallery_value->custom_caption }}</textarea>
                                        </div>
                                        <div class="file-manager-data-{{md5($gallery_value->id)}}">
                                            <div class="{{ json_decode(config('constant.CONFIG_GALLERY_IMAGE_ARTICLE'))->key }}">
                                                <img src="{{Helpers::Image(config('constant.ASSETS_URL').'uploads/img/original/'.$gallery_value->file_manager->filename)}}" width="200px">
                                                <textarea style="margin-top:20px;width:100%;height:100px;" placeholder="Image Caption" name="custom_caption_{{md5($gallery_value->id)}}" id="custom_caption">{{ $gallery_value->custom_caption }}</textarea>
                                            </div>
                                            <input type="hidden" name="file_manager_id_{{md5($gallery_value->id)}}" value="{{$gallery_value->file_manager_id}}">
                                            <input type="hidden" name="image_data_{{md5($gallery_value->id)}}" value="{{$gallery_value->file_manager->filename}}">
                                            <input type="hidden" name="crop" value="false">
                                            <input type="hidden" name="config_{{md5($gallery_value->id)}}" value="{{$config_modified}}">
                                        </div><br>
                                        <a href="#" onclick="file_manager_show('{{$config_modified}}')" class="btn btn-md btn-primary">Select Image</a>
                                        <a class="btn btn-md btn-danger" onclick="remove_div('{{md5($gallery_value->id)}}')">Remove</a>
                                        <input type="hidden" name="other_image[]" value="{{md5($gallery_value->id)}}">
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
        </form>
    </div>
</div>
@endsection