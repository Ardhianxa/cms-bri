<div class="form-group col-sm-12 col-md-12 col-lg-3 {{$div_id}}">
    <label>&nbsp</label><br>
    <div class="file-manager-data-{{$div_id}}-default-image">
        <img src="{{config('constant.ASSETS_URL')}}backend/img/example-image.jpg" width="200px">
    </div>
    <div class="file-manager-data-{{$div_id}}"></div><br>
    <a href="#" onclick="file_manager_show('{{$config_modified}}')" class="btn btn-md btn-primary">Select Image</a>
    <a class="btn btn-md btn-danger" onclick="remove_div('{{$div_id}}')">Remove</a>
    <input type="hidden" name="other_image[]" value="{{$div_id}}">
</div>