<?php
namespace App\Modules\Article\Models; 

require (__DIR__.'/../Helpers/Fungsi.php'); //lebih prioritas helpers di master 
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Article\Models\Content_tagging;

class Tagging extends Model
{
    protected $table = 'tagging';
    protected $primaryKey = 'id';

    protected $fillable = [
    ];

    protected $hidden = [
    ];

    public static function savedata($article_id,$tagging)
    {
        //delete first
        $delete = Tagging::deleteTagging($article_id);

        if(count($tagging) > 0){
            foreach($tagging as $tagging_value){
                $tagging_id = Tagging::savetagging($tagging_value);
                $save_tagging = Content_tagging::savedata($tagging_id,$article_id);
            }
        }
    }

    public static function deleteTagging($artikel_id)
    {
        return Content_tagging::where('content_id',$artikel_id)->delete();
    }

    public static function savetagging($tagging)
    {
        $id = "";
        if($tagging != ""){
            $tag = Self::select('id')->where('name',$tagging)->first();
            if(isset($tag->id)){
                $id = $tag->id;
            }
            else{
                $new = new Tagging();
                $new->name = $tagging;
                $new->slug = str_replace(" ","-",$tagging);
                $new->status = 1;
                $new->save();

                $id = $new->id;
            }
        }

        return $id;
    }
}
