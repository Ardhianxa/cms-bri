<?php
namespace App\Modules\Article\Models; 

require (__DIR__.'/../Helpers/Fungsi.php'); //lebih prioritas helpers di master 
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;

class Content_tagging extends Model
{
    protected $table = 'content_tagging';
    protected $primaryKey = 'id';

    protected $fillable = [
    ];

    protected $hidden = [
    ];

    public static function savedata($tagging_id,$article_id)
    {
        if($tagging_id != ""){
            $save = new Content_tagging();
            $save->content_id = $article_id;
            $save->tagging_id = $tagging_id;
            $save->status = 1;
            $save->save();
        }

        return true;
    }

    public function Data()
    {
        return $this->hasOne('App\Modules\Article\Models\Tagging', 'id', 'tagging_id')
            ->select(['id', 'name', 'slug']);
    }
}
