<?php
Route::group(  
[
	'namespace' => 'App\Modules\Article\Controllers', 
	'prefix' => '', 
	'middleware' => [
						'web',
						'session',
						'auth',
						'before_global',
						'after_global',
						'App\Modules\Article\Middleware\BeforeMiddleware',
						'App\Modules\Article\Middleware\AfterMiddleware'
					]
], 
function ()
{
	Route::get('article/add', [
		'as' => 'article-add', 'uses' => 'ArticleController@add', 'middleware' => ['permission:add article']
	]);

	Route::get('article/add-other-image', [
		'as' => 'article-add-other-image', 'uses' => 'ArticleController@add_other_image'
	]);

	Route::post('article/add', [
		'as' => 'article-add-post', 'uses' => 'ArticleController@store', 'middleware' => ['permission:add article']
	]);
	
	Route::get('article/{status}/{key?}', [
		'as' => 'article', 'uses' => 'ArticleController@index', 'middleware' => ['permission:view article']
	]);

	Route::get('article/edit/{id}/{status}', [
		'as' => 'article-edit', 'uses' => 'ArticleController@edit', 'middleware' => ['permission:edit article']
	]);

	Route::post('article/edit/{id}/{status}', [
		'as' => 'article-edit-post', 'uses' => 'ArticleController@update', 'middleware' => ['permission:edit article']
	]);

	Route::post('article/delete/{status}/{id}', [
		'as' => 'article-delete', 'uses' => 'ArticleController@delete', 'middleware' => ['permission:delete article']
	]);

	// Route::post('channel/post', [
	// 	'as' => 'channel-post', 'uses' => 'ChannelController@post', 'middleware' => ['permission:add channel']
	// ]);
});
?>