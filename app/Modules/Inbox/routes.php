<?php
Route::group(  
[
	'namespace' => 'App\Modules\Inbox\Controllers', 
	'prefix' => '', 
	'middleware' => [
						'web',
						'auth',
						'session',
						'before_global',
						'after_global',
						'App\Modules\Inbox\Middleware\BeforeMiddleware', 
						'App\Modules\Inbox\Middleware\AfterMiddleware',
					]
], 
function ()
{
	Route::get('inbox/{id?}', [
		    'as' => 'inbox', 'uses' => 'InboxController@index', 'middleware' => []
	]);

	Route::get('ajax-get-receiver-message', [
		    'as' => 'ajax-get-receiver-message', 'uses' => 'InboxController@ajax_get_receiver_message', 'middleware' => []
	]);

	Route::get('inbox/save/{sender_id}/{received_id}', [
		    'as' => 'save-chat', 'uses' => 'InboxController@chat_save', 'middleware' => []
	]);

	Route::post('inbox/new', [
		    'as' => 'new-chat', 'uses' => 'InboxController@new', 'middleware' => []
	]);
});

Route::get('ajax-messengeers/{id?}', [
	    'as' => 'ajax-messengeers', 'uses' => 'App\Modules\Inbox\Controllers\InboxController@messengeers', 'middleware' => ['web']
]);
?>