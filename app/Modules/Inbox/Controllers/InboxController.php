<?php  
 
namespace App\Modules\Inbox\Controllers;  

require (__DIR__.'/../Helpers/Fungsi.php'); //lebih prioritas helpers di master 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\Inbox\Models\Inbox;
use Session;
use Auth;

class InboxController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    } 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request, $id = null) 
    {
        $error = false;
        $title = 'Inbox';
        $page = 'Inbox::data';
        $messages = Inbox::Data();

        if($id != null){
            $exists = Inbox::exists($id);
            if(!isset($exists->id)){
                $error = true;
            }
            else{
                $exists->baca = 1;
                $exists->save();
            }
        }
        
        if($error){
            return view('errors.404');
        }
        else{
            return view($page,compact('title','page','messages','id'));
        }
    }

    public function ajax_get_receiver_message(Request $request)
    {
        $users = Inbox::Receiver($request->q);
        echo $users;
    }

    public function messengeers(Request $request, $id = null)
    {
        $messages = Inbox::Data();
        return view('Inbox::messengeers',compact('messages','id'));
    }

    public function chat_save(Request $request)
    {
        $data = Inbox::select('id')->where('sender_id',$request->sender_id)->where('received_id',$request->received_id)->first();
        if(isset($data->id)){
            $data->status = 1;
            $data->baca = 2;
            $data->save();
        }
        else{
            $new = new Inbox();
            $new->sender_id = $request->sender_id;
            $new->received_id = $request->received_id;
            $new->status = 1;
            $new->baca = 2;
            $new->save();
        }

        echo 'ok';
    }

    public function new(Request $request)
    {
        $received_id = $request->received_id;

        //as sender
        $request->sender_id = Auth::id();
        $request->received_id = $request->received_id;
        Self::chat_save($request);

        //as receiver
        $request->sender_id = $request->received_id;
        $request->received_id = Auth::id();
        Self::chat_save($request);

        return redirect()->route('inbox',['id' => $received_id]);
    }
}