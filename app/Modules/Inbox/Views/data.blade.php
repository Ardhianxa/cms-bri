@extends('layouts.app')   

@section('extend_code')
	<script>
	$("#modal-new-message").fireModal({ 
	  title: 'New Message',
	  body: $("#modal-login-part"),
	  footerClass: 'bg-whitesmoke',
	  autoFocus: false,
	  onFormSubmit: function(modal, e, form) {
	    
	  },
	  shown: function(modal, form) {
	    
	  },
	  buttons: [
	    {
	      text: 'Create',
	      submit: true,
	      class: 'btn btn-primary btn-shadow',
	      handler: function(modal) {
	      }
	    }
	  ]
	});
	</script>
@endsection

@section('content')
	<div class="card2 row">
		<div class="col-12 col-sm-12 col-md-12 col-lg-3">
			<div class="card">
                <div class="card-body">
                    <a class="btn btn-md btn-primary" href="#" id="modal-new-message">NEW MESSAGE</a>
                </div>
                <div class="card-body box-messengeers" id="list-users">
                    @include('Inbox::messengeers')
                </div>
            </div>
		</div>
		<div class="col-sm-12 col-md-9 col-lg-9" style="margin-bottom:100px;background-image: url('{{ config('constant.ASSETS_URL') }}backend/img/chat.png');">
			@if($id != null)
			<div class="unloading">
				<div id="chatws">
	              	<div class="chat">
		                <div id='message_box'></div>
		                <form id="msg_form" style="margin-top:-10px;">
		                	<div class="input-group mb-3">
		                		<textarea id="message" class="form-control" placeholder="Message.."></textarea>
		                        <div class="input-group-append">
		                        	<button type="submit" id="save" class="btn btn-lg btn-primary"><i class="fas fa-paper-plane"></i></button>
		                        </div>
	                      	</div>
		                  	<br><br>
		               	</form>
	              	</div>
	            </div>
        	</div>
            @endif
		</div>
	</div>
	@include('Inbox::js')
  	@include('Inbox::add-modal')
@endsection