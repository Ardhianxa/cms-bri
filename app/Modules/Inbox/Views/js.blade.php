@section('extend_code_2') 

<script src="https://cdn.firebase.com/js/client/2.2.3/firebase.js"></script>
<script>
setInterval(function(){
    $("#list-users").load("{{ route('ajax-messengeers',['id' => $id]) }}");
}, 3000) //2 detik

var dbRef = new Firebase("{{ config('constant.FIREBASE_CHAT') }}");
if(parseInt('{{ Auth::id() }}') >= parseInt('{{ $id }}')){
	var idGab = '{{ $id }} - {{ Auth::id() }}';
}
else{
	var idGab = '{{ Auth::id() }} - {{ $id }}';
}

var chatsRef = dbRef.child(idGab);
chatsRef.on("child_added", function(snap) {
  document.querySelector('#message_box').innerHTML += (chatHtmlFromObject(snap.val()));
});

//save chat
if($("#save").length > 0)
{
	document.querySelector('#save').addEventListener("click", function(event) {
	  	var a = new Date(),
	  	b = a.getDate(),
	  	c = a.getMonth(),
	  	d = a.getFullYear(),
	  	date = b + '/' + c + '/' + d, chatForm = document.querySelector('#msg_form');
	  	event.preventDefault();
	  	if (document.querySelector('#message').value != '') 
	  	{
		  	chatsRef.push({
  		 	  id:'0',
  		  	name: "{{ Session::get('first_name').' '.Session::get('last_name') }}",
  		  	key:'{{ Auth::id() }}',
  		  	message: document.querySelector('#message').value,
  		  	date: date
		  	})
		  	chatForm.reset();

		  	//save to db
		  	$.get("{{ route('save-chat',['sender_id' => Auth::id(), 'received_id' => $id ]) }}",function(data){

		  	});
	  	} 
	}, false);
}

function chatHtmlFromObject(chat) 
{
		var bubble = (chat.key == '{{ Auth::id() }}' ? "bubble-right" : "bubble-left");
		var html = '<div class="' + bubble + '"><p><span class="msgc">' + chat.message + '</span><span style="margin-top:-10px;margin-bottom:-10px;" class="date">' + chat.date + '</span></p></div>';

		return html;
}
</script>

<script type="text/javascript" src="{{ config('constant.ASSETS_URL') }}backend/js/select2.js"></script>
<script type="text/javascript">
    $.fn.select2.amd.require([
      "select2/core",
      "select2/utils",
      "select2/compat/matcher"
    ], function (Select2, Utils, oldMatcher) {
      var $receiver = $(".receiver");
      $.fn.select2.defaults.set("width", "100%");
      $.fn.modal.Constructor.prototype._enforceFocus = function() {};
      $receiver.select2({

        ajax: {
        	url: "{{ route('ajax-get-receiver-message') }}",
        	type: "GET",
          	dataType: 'json',
          	delay: 250,
          	data: function (params) {
	            return {
	              q: params.term,
	              page: params.page
	            };
          	},
          	processResults: function (data, params) {
	            document.getElementById("receiver").selectedIndex = 0;
	            params.page = params.page || 1;
	            return {
	              results: data.items,
	              pagination: {
	              more: (params.page * 30) < data.total_count
	              }
	            };
          	},
          	cache: true
        },
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 1,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
      });

      function formatRepo (repo) {
        if (repo.loading) return repo.text;

        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__title'>" + repo.first_name +" "+ repo.last_name +" ("+ repo.privileges +")</div>";

        markup += "</div>";

        return markup;
      }

      function formatRepoSelection (repo) {
        if(repo.first_name==null)
        {
          return repo.teks || repo.text;
        }
        else
        {
          return repo.first_name + " " + repo.last_name + "("+repo.privileges+")";
        }
      }

      });
  </script>
@endsection