<ul class="list-unstyled list-unstyled-border">  
	@foreach($messages as $message)
		@if($message->sender_id==$id)
			@php
				$class_div = 'panel-content-active';
			@endphp
		@else
			@if($message->baca == 2)
				@php
    				$class_div = 'panel-content-unread';
    			@endphp
			@else
				@php
    				$class_div = 'panel-content-inactive';
    			@endphp
			@endif
		@endif
		<a href="{{ route('inbox',['id' => $message->sender_id]) }}">
		<div class="{{ $class_div }}">
		<li class="media panel-id-<?=$message->sender_id?>" style="">
            <img alt="image" class="mr-3 rounded-circle" width="50" src="{{ Helpers::Image(config('constant.ASSETS_URL').'backend/img/users/'.$message->users_info->image) }}">
            <div class="media-body">
              <div class="mt-0 mb-1 font-weight-bold">{{ ucwords(strtolower($message->users_info->first_name).' '.strtolower($message->users_info->last_name)) }}</div>
              <div class="text-small font-weight-600 text-muted">
              	@foreach($message->privileges as $privilege)
              		<i class="fas fa-circle"></i> {{ $privilege.' ' }}
              	@endforeach
              </div>
            </div>
            <div style="display:none;" class="status-active-<?=$message->sender_id?>">nonactive</div>
        </li>
    	</div>
    	</a>
	@endforeach
</ul>