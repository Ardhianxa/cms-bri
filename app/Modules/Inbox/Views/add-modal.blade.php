<form method="POST" class="modal-part" id="modal-login-part" action="{{ route('new-chat') }}">
	@csrf
    <div class="form-group">
        <label>Receiver</label>
        <div class="input-group">
          	<select required name="received_id" id="receiver" class="receiver">
                <option value="">-Choose-</option>
            </select>
        </div>
    </div>
</form>