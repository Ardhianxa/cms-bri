<?php

namespace App\Modules\Inbox\Models; 

use Illuminate\Database\Eloquent\Model; 
use App\User;
use Auth;
use Illuminate\Support\Facades\Cache;

class Inbox extends Model 
{
    /*
    
    KONSEP CACHE NAME
    NAME_NAME_USERID_NAME_NAME-N

    */

    protected $table = 'inbox';
    protected $primaryKey = 'id';

    protected $fillable = [
        'sender_id',
		'received_id',
        'body',
        'attachment',
        'status'
    ];

    protected $hidden = [
    ];

    public static function unread($id)
    {
        $read = Inbox::select('id')->where('sender_id',$id)->where('received_id',Auth::id())->first();
        if(isset($read->id)){
            $read->baca = 1;
            $read->save();
        }
        
        return true;
    }

    public static function exists($id)
    {
        if(config('constant.CACHE')){
            $cache_name = "inbox_exists_".Auth::id();
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::exists_data($id), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::exists_data($id);
        }

        return $data;
    }

    public static function exists_data($id)
    {
        return Self::select('id')->where('status',1)->where('sender_id',$id)->where('received_id',Auth::id())->first();
    }

    public static function Receiver_data($q)
    { 
        $aktif = Self::Data();
        $id_aktif = array();
        if(isset($aktif[0]->sender_id)){
            foreach($aktif as $aktif_value){
                $id_aktif[] = $aktif_value->sender_id;
            }
        }

        $users = User::select('users.id as id','first_name','last_name')
        ->join('users_info','users_info.user_id','=','users.id')->whereNotIn('users.id',$id_aktif)
        ->whereRaw("
            (users_info.first_name LIKE '%".$q."%' or users_info.last_name LIKE '%".$q."%')
            ")
        ->where('users.id','!=',Auth::id())
        ->where('users_info.status',1)
        ->orderBy('users_info.first_name','asc')
        ->get();

        
        $users->transform(function($users){
            $users->privileges = $users->getRoleNames();
            return $users;
        });

        $data = array();
        foreach($users as $value)
        {
            $data[] = $value;
        }

        $ddataA['total_count'] = 2;
        $ddataA['incomplete_results'] = false;
        $ddataA['items'] = $data;
        $json = json_encode($ddataA);

        return $json;
    }

    public static function Receiver($q)
    {   
        if(config('constant.CACHE')){
            $cache_name = "inbox_receiver_".Auth::id()."_".$q;
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::Receiver_data($q), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::Receiver_data($q);
        }

        return $data;
    }

    public static function Data()
    {
        if(config('constant.CACHE')){
            $cache_name = "inbox_list_".Auth::id();
            if(!Cache::has($cache_name)) {
                Cache::put($cache_name, Self::Data_data(), now()->addMinutes(config('constant.CACHE_TIME')));
            }
            $data = Cache::get($cache_name);
        }
        else{
            $data = Self::Data_data();
        }

        return $data;
    }

    public function Users_info()
    {
        return $this->hasOne('App\Http\Models\UsersInfo', 'user_id', 'sender_id')
            ->select(['id', 'user_id', 'image', 'first_name','last_name', 'status']);
    }

    public function Users()
    {
        return $this->hasOne('App\Http\Models\Users', 'id', 'sender_id')
            ->select(['id', 'name']);
    }

    public static function Data_data()
    {
        $data = Self::
            select('baca','id','sender_id','received_id','created_at')
            ->with('Users','Users_info')
            ->where('received_id',Auth::id())
            ->orderBy('baca','desc') 
            ->orderBy('updated_at','desc') 
            ->get();

        $data = $data->filter(function($item){
            return $item->users_info->status === 1;
        })->values();

        $data->transform(function($data){
            $user = User::select('id')->where('id',$data->sender_id)->first();
            $data->privileges = $user->getRoleNames();
            return $data;
        });

        return $data;
    }
}
