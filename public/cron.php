<?php
ob_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define("BASEPATH", "");
define('DEFAULT_SERVER', "192.168.2.3");
define('DEFAULT_USERNAME', "root" );
define('DEFAULT_PASSWORD', "masuk" );
define('DEFAULT_NAME', "cms_new" );
define('DEFAULT_NAME_PREFIX', '');
define('DEFAULT_DRIVER', 'mysql');
define('DEFAULT_DSN', DEFAULT_DRIVER.':host='.DEFAULT_SERVER.';dbname='.DEFAULT_NAME);

try 
{
    $database = new PDO(DEFAULT_DSN,DEFAULT_USERNAME,DEFAULT_PASSWORD);
    $database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e)
{
    echo $e->getMessage();
}

$data = $database->prepare("select lokasi_ID, lokasi_propinsi, lokasi_kabupatenkota, lokasi_kecamatan, lokasi_kelurahan from inf_lokasi where lokasi_propinsi != 00 and lokasi_kabupatenkota != 00 and lokasi_kecamatan != 00 and lokasi_kelurahan != 0000 limit 1000");
$data->execute();
$inf = $data->fetchAll();

foreach ($inf as $inf_data) {

    $lokasi_id = $inf_data['lokasi_ID'];
    $lokasi_propinsi = $inf_data['lokasi_propinsi'];
    $lokasi_kabupatenkota = $inf_data['lokasi_kabupatenkota'];
    $lokasi_kecamatan = $inf_data['lokasi_kecamatan'];
    $lokasi_kelurahan = $inf_data['lokasi_kelurahan'];

    //provinsi name
    $provinsi = $database->prepare("select lokasi_nama from inf_lokasi where lokasi_propinsi = '$lokasi_propinsi' and lokasi_kabupatenkota = 00 and lokasi_kecamatan = 00 and lokasi_kelurahan = 0000");
    $provinsi->execute();
    $provinsi = $provinsi->fetchColumn();

    //kabupaten name
    $kabupaten = $database->prepare("select lokasi_nama from inf_lokasi where lokasi_propinsi = '$lokasi_propinsi' and lokasi_kabupatenkota = '$lokasi_kabupatenkota' and lokasi_kecamatan = 00 and lokasi_kelurahan = 0000");
    $kabupaten->execute();
    $kabupaten = $kabupaten->fetchColumn();

    //kecamatan name
    $kecamatan = $database->prepare("select lokasi_nama from inf_lokasi where lokasi_propinsi = '$lokasi_propinsi' and lokasi_kabupatenkota = '$lokasi_kabupatenkota' and lokasi_kecamatan = '$lokasi_kecamatan' and lokasi_kelurahan = 0000");
    $kecamatan->execute();
    $kecamatan = $kecamatan->fetchColumn();

    //kelurahan name
    $kelurahan = $database->prepare("select lokasi_nama from inf_lokasi where lokasi_propinsi = '$lokasi_propinsi' and lokasi_kabupatenkota = '$lokasi_kabupatenkota' and lokasi_kecamatan = '$lokasi_kecamatan' and lokasi_kelurahan = '$lokasi_kelurahan'");
    $kelurahan->execute();
    $kelurahan = $kelurahan->fetchColumn();

    //find
    $find = $database->prepare("select kodepos from tbl_kodepos where provinsi = '$provinsi' and kabupaten = '$kabupaten' and kecamatan = '$kecamatan' and kelurahan = '$kelurahan'");
    $find->execute();
    $find = $find->fetchColumn();

    if($find != ""){
        // $update_sql = "UPDATE inf_lokasi SET kodepos=? WHERE id=?";
        // $update = $database->prepare($update_sql);
        // $update->execute([$find, $lokasi_id]);
    }
}