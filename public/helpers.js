function jembut(url)
    {
      window.location = url;
    }

function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which; 
    if (event.keyCode === 8) {
        return true;
    } else if ( key < 48 || key > 57 ) {
        return false;
    } else {
        return true;
    }
}

function validateDateField(event)
{
  event.preventDefault();
}

function rupiah(bilangan)
{
    var reverse = bilangan.toString().split('').reverse().join(''),
    ribuan = reverse.match(/\d{1,3}/g);
    ribuan = ribuan.join('.').split('').reverse().join('');
    
    return "Rp " + ribuan;
}

function rupiah2(bilangan)
{
    var reverse = bilangan.toString().split('').reverse().join(''),
    ribuan = reverse.match(/\d{1,3}/g);
    ribuan = ribuan.join(',').split('').reverse().join('');

    return ribuan;
}

function confirmDelete(url)
{
    swal({
      title: "Are you sure ?",
      // text: "You will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
      window.location = url;
    });
}

window.jembut = jembut;