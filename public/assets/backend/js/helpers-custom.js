let markers = [];
/*USER*/

function remove_div(id)
{
    $("."+id).remove();
}

function article_add_other_image(route,div)
{
    $.ajax({
    url: route,
    type: "get",
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    data: {
    },
    beforeSend: function () {
      
    },
    error: function() {
      
    },
    success: function (html) {
      $(html).hide().appendTo("."+div).fadeIn(1000);
    }
  });
}

function ajax_get_data(page,key,status,div,status2,route)
{
  $.ajax({
    url: route,
    type: "get",
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    data: {
      page: page,
      key: key,
      status : status,
      status2 : status2
    },
    beforeSend: function () {
      $("."+div).html("<div class='text-center'><br><div class='fa fa-spinner fa-spin fa-3x'></div></div>");
    },
    error: function() {
      $("."+div).html("ERROR");
    },
    success: function (html) {
      $("."+div).html("");
      $("."+div).append(html);
      //$(html).hide().appendTo("."+div).fadeIn(1000);
    }
  });
}

/*FRONTEND*/
function tujuan(id)
{
    if(id==1){
        $(".judul option[value=2]").remove();
    }
    else{
        var optionExists = ($('.judul option[value=2]').length > 0);
        if(!optionExists)
        {
            $(".judul").append(new Option("Ketersediaan Produk", 2));
        }
    }
}

function consumer_letter_type(id)
{
  $(".additional").hide();
  $(".additional2").hide();
  $(".additional3").hide();
    if(id==3)
    {
        $(".additional").show();
        $(".additional2").show();
        $(".additional3").show();
    }
    else if(id==2)
    {
        $(".additional2").show();
        $(".additional3").show();
    }
}

function get_kabupaten(value,div,route){
  $.ajax({
      url: route,
      type: "get",
      headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data: {
          provinsi_data: value
      },
      beforeSend: function () {
          
      },
      error: function() {
          
      },
      success: function (html) {
          $("."+div).html("");
          $("."+div).append(html);
      }
  });
}

function get_kecamatan(provinsi,value,div,route){
    $.ajax({
        url: route,
        type: "get",
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            kabupaten_data: value,
            provinsi_data: provinsi
        },
        beforeSend: function () {
            
        },
        error: function() {
            
        },
        success: function (html) {
            $("."+div).html("");
            $("."+div).append(html);
        }
    });
}

function get_kodepos(provinsi,kabupaten,value,div,route){
  $.ajax({
      url: route,
      type: "get",
      headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data: {
          kabupaten_data: kabupaten,
          provinsi_data: provinsi,
          kecamatan_data:value
      },
      beforeSend: function () {
          
      },
      error: function() {
          
      },
      success: function (html) {
          $("."+div).html("");
          $("."+div).append(html);
      }
  });
}

function initAutocomplete() {
    if(navigator.geolocation) 
    {
        navigator.geolocation.getCurrentPosition(function (p)
        {
            var currenct_LatLng = new google.maps.LatLng(p.coords.latitude, p.coords.longitude);
            //var currenct_LatLng = { lat: -33.8688, lng: 151.2195 };
            const map = new google.maps.Map(document.getElementById("map_canvas"), {
                center: currenct_LatLng,
                mapTypeControl: true,
                mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
                navigationControl: true,
                zoom: 13,
                mapTypeId: "roadmap",
            });
            // Create the search box and link it to the UI element.
            const input = document.getElementById("pac-input");
            const searchBox = new google.maps.places.SearchBox(input);
            //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            // Bias the SearchBox results towards current map's viewport.
            map.addListener("bounds_changed", () => {
            searchBox.setBounds(map.getBounds());
            });
            
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener("places_changed", () => {
            const places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }
            // Clear out the old markers.
            markers.forEach((marker) => {
                marker.setMap(null);
            });

            // For each place, get the icon, name and location.
            const bounds = new google.maps.LatLngBounds();
            places.forEach((place) => {
                if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
                }
                const icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25),
                };
                
                createMarker(map,place.geometry.location, "<font color='#000000'><b>Location</b></font><br><font color='#000000'>"+place.geometry.location+"</font>", "","marker");

                if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
                } else {
                bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
            });
            
            google.maps.event.addListener(map, 'click', function(event) {
                marker = createMarker(map,event.latLng, "<font color='#000000'><b>Location</b></font><br><font color='#000000'>"+event.latLng+"</font>", "","marker");
            });

            var ikonCurrent = assets_url+"frontend/images/location.png";
            var marker = createMarker(map,currenct_LatLng,"<font color='#000000'><b>Your Location</b></font>",ikonCurrent,"current");
        });
    }
    else
    {
        alert('Geo Location feature is not supported in this browser.');
    }
}

function createMarker(map, latlng, html, ikon, type) 
{
    markers.forEach((marker) => {
        marker.setMap(null);
    });

    if(type=='marker')
    {
        ambilKoordinate(latlng);
    }
                    
    var contentString = html;
    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        icon: ikon,
        zIndex: Math.round(latlng.lat()*-100000)<<5
    });

    var infowindow = new google.maps.InfoWindow(
	{ 
		size: new google.maps.Size(200,100)
	});

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(contentString); 
        infowindow.open(map,marker);
    });

    if(type=='marker'){
        google.maps.event.trigger(marker, 'click');  
        
        markers.push(
            marker
        );

        return marker;
    }
}

function ambilKoordinate(latlng)
{
    document.getElementById("latlong").value = latlng;
}

/*CHANNEL*/
function channel_add(parent_id,parent_text,level,id)
{
    $('.category-title').html('');
    $('.category-status').html('');
    $('.parent-id').html('');
    $('.parent-text').html('');

    $('.title').val("");
    $('.slug').val("");
    $('.meta_title').val("");
    $('.meta_description').val("");

    $('.category-title').html('ADD Channel');
    $('.category-status').html("<label class='custom-switch mt-2'><input type='checkbox' value='1' name='status' class='custom-switch-input' checked style='width:200px;'><span class='custom-switch-indicator'></span><span class='custom-switch-description'>Active</span></label>");
    $('.parent-id').html("<input type='hidden' name='parent_id' value='"+parent_id+"'>");
    $('.category-level').html("<input type='hidden' name='level' value='"+level+"'>");
    if(parent_text != null){
        $('.parent-text').html("<h6>"+parent_text+"</h6>");
    }
    $('.modal-category-add').modal('show');
}

function channel_edit(route,parent_id,parent_text,level,id)
{
    $.ajax({
        url: route,
        type: "get",
        data: {
            id : id
        },
        beforeSend: function () {
        
        },
        error: function() {
        
        },
        success: function (html) {
            $('.category-title').html('');
            $('.category-status').html('');
            $('.parent-id').html('');
            $('.parent-text').html('');
            $('.category-title').html('EDIT Channel');
            if(parent_text != null){
                $('.parent-text').html("<h6>"+parent_text+"</h6>");
            }
            $('.category-id').html("<input type='hidden' name='id' value='"+id+"'>");
            $('.parent-id').html("<input type='hidden' name='parent_id' value='"+parent_id+"'>");
            $('.category-level').html("<input type='hidden' name='level' value='"+level+"'>");

            var json = JSON.parse(html);
            $('.title').val(json.name);
            $('.slug').val(json.slug);
            $("textarea.meta_title").val(json.meta_title);
            $("textarea.meta_description").val(json.meta_description);

            if(json.status == 1){
                var checked = "checked";
            }
            else{
                var checked = "";
            }
            $('.category-status').html("<label class='custom-switch mt-2'><input type='checkbox' value='1' name='status' class='custom-switch-input' "+checked+" style='width:200px;'><span class='custom-switch-indicator'></span><span class='custom-switch-description'>Active</span></label>");
            
            $('.modal-category-add').modal('show');
        }
    });
}