function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which; 
    if (event.keyCode === 8) {
        return true;
    } else if ( key < 48 || key > 57 ) {
        return false;
    } else {
        return true;
    }
}

function clear_box_image(config) 
{
  var json = JSON.parse(config);
  $("."+json.container).html("");
  $("."+json.container+"-default-image").show();
}
  
function redirect(url) 
{
  window.location = url;
}

function validateDateField(event)
{
  event.preventDefault();
}

function rupiah(bilangan)
{
    var reverse = bilangan.toString().split('').reverse().join(''),
    ribuan = reverse.match(/\d{1,3}/g);
    ribuan = ribuan.join('.').split('').reverse().join('');
    
    return "Rp " + ribuan;
}

function confirmDelete(url) 
{
    swal({
      title: "Are you sure ?",
      // text: "You will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
        url: url,
        type: "post",
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            action : 'delete'
        },
        beforeSend: function () {
            
        },
        error: function() {
            
        },
        success: function (html) {
            window.location = html;
        }
      });
    });
}

function notif(text,act)
{
  sweetAlert("Oops...", text, act);
}

function sweet_alert(text,act)
{
  sweetAlert("Oops...", text, act);
}